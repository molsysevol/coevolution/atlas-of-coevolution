# Created on 04/03/20 by jdutheil

# Depending on the analyses, we use two type of data: casted (one property per column) or molted (all properties in lines, one column named "Method" to distinguish them).
# The "casted" representation as one unique line per site, but many columns.
# The "molted" representation as one line per unique site-property combination, and less columns.

#### Compare methods ####

require(plyr)
require(ggplot2)
require(ggpubr)
require(ggrepel)
require(cowplot)
require(scales)

datc.full <- read.table("AllFamilies_sites_casted.csv.gz", header = TRUE, sep = ",", stringsAsFactors = FALSE)
#indices <- c("ind1", "ind2", "ind3", "ind4", "ind5", "ind6", "ind7", "ind8", "vol", "vol3", "pol", "pol3", "charge")

# Number of sites:
length(unique(paste(datc.full$Group, datc.full$Family))) #366794
nrow(datc.full) # Should be the same!

# Number of families:
length(unique(datc.full$Family)) #1630

# Original dataset (no discretized variables):
datc <- datc.full[,1:48]
indices <- c("ind1", "ind2", "ind3", "ind4", "ind5", "ind6", "ind7", "ind8", "vol", "pol", "charge")

# Number of coevolving residues:
sets <- !is.na(datc[,paste("Size.", indices, sep = "")])
sets[sets] <- 1
sets[!sets] <- 0
sets <- as.data.frame(sets)
names(sets) <- c(paste("I", 1:8, sep = ""), "Volume", "Polarity", "Charge")
isCoevolving <- apply(sets, 1, sum) > 0
table(isCoevolving)
#FALSE   TRUE
#315133  51661
sum(isCoevolving)*100/length(isCoevolving) #14.08%
datc$IsCoevolving <- isCoevolving

# Look at overlap:
require(UpSetR)
p <- upset(sets, sets = names(sets), number.angle = 30, keep.order = TRUE, nintersects = 33)
p

# Save to file
s<-0.6
pdf(file="Upset.pdf", onefile=FALSE, width=16*s, height = 10*s)
p
dev.off()

# Number of detected sites per method:
apply(sets, 2, sum)

#Alternative:
upset(sets, sets = names(sets), number.angle = 30, keep.order = TRUE, group.by = "sets", cutoff = 5, nintersects = NA)

# Compare to a random expectation:
sets.rnd <- data.frame(
  I1 = sample(sets$I1),
  I2 = sample(sets$I2),
  I3 = sample(sets$I3),
  I4 = sample(sets$I4),
  I5 = sample(sets$I5),
  I6 = sample(sets$I6),
  I7 = sample(sets$I7),
  I8 = sample(sets$I8),
  Volume = sample(sets$Volume),
  Polarity = sample(sets$Polarity),
  Charge = sample(sets$Charge)
)
upset(sets.rnd, sets = names(sets.rnd), number.angle = 30, keep.order = TRUE, nintersects = 33)
upset(sets.rnd, sets = names(sets.rnd), number.angle = 30, keep.order = TRUE, group.by = "sets", cutoff = 5, nintersects = NA)

randomise.sets <- function(sets, nsim, thresholds = 2:11) {
  s <- apply(sets, 1, sum)
  obs <- sapply(thresholds, function(x) sum(s >= x))
  pb <- txtProgressBar(0, nsim, style = 3)
  sims <- matrix(nrow = nsim, ncol = length(thresholds))
  for (i in 1:nsim) {
    setTxtProgressBar(pb, i)
    sets.rnd <- data.frame(
      I1 = sample(sets$I1),
      I2 = sample(sets$I2),
      I3 = sample(sets$I3),
      I4 = sample(sets$I4),
      I5 = sample(sets$I5),
      I6 = sample(sets$I6),
      I7 = sample(sets$I7),
      I8 = sample(sets$I8),
      Volume = sample(sets$Volume),
      Polarity = sample(sets$Polarity),
      Charge = sample(sets$Charge)
    )
    s <- apply(sets.rnd, 1, sum)
    sims[i,] <- sapply(thresholds, function(x) sum(s >= x))
  }

  return(list(thresholds = thresholds, sims = sims, obs = obs))
}

# We get the random distribution of the number of method intersections:
sim1 <- randomise.sets(sets, 10000, 2:11)
save(sim1, file = "sim1.Rdata")

plot.sims <- function(sims, which.threshold = 2) {
  require(ggplot2)
  i <- which(sims$thresholds == which.threshold)
  p <- ggplot(data.frame(x = sims$sims[,i]), aes(x = x, y = ..density..)) +
    geom_histogram() +
    geom_vline(xintercept = sims$obs[i])
  return(p)
}

plot.sims(sim1, 2)
plot.sims(sim1, 3)
plot.sims(sim1, 4)
# etc
plot.sims(sim1, 11)
# Always significant



# Condition over the site specific substitution rate:

get.sets <- function(datc, rate.threshold, coev.threshold) {
  filter <- apply(datc[,paste("Np.", indices, sep = "")], 1, function(x) any(x >= rate.threshold))
  sets <- !is.na(datc[filter, paste("Size.", indices, sep = "")])
  sets[sets] <- 1
  sets[!sets] <- 0
  sets <- as.data.frame(sets)
  names(sets) <- c(paste("I", 1:8, sep = ""), "Volume", "Polarity", "Charge")
  isCoevolving <- apply(sets, 1, sum) >= coev.threshold
  return(sum(isCoevolving)*100/length(isCoevolving))
}

minrates <- seq(0, 10, by = 0.5)
mincoev <- 1:11
n <- length(minrates)
m <- length(mincoev)
propcoev <- matrix(nrow = n, ncol = m)
col.x <- numeric(n*m)
col.y <- numeric(n*m)
col.z <- numeric(n*m)
pb <- txtProgressBar(0, n*m, style = 3)
for (i in 1:n) {
  for (j in 1:m) {
    setTxtProgressBar(pb, (i-1)*m+j)
    propcoev[i,j] <- get.sets(datc, rate.threshold = minrates[i], coev.threshold = mincoev[j])
    col.x[(i-1)*m+j] <- minrates[i]
    col.y[(i-1)*m+j] <- mincoev[j]
    col.z[(i-1)*m+j] <- propcoev[i,j]
  }
}
dat.propcoev <- data.frame(MinRate = col.x, MinCoev = col.y, PropCoev = col.z)

contour(log(propcoev), x = minrates, y = mincoev)
image(log(propcoev), x = minrates, y = mincoev)

library(plotly)
plot_ly(z = dat.propcoev$PropCoev,
        x = dat.propcoev$MinRate,
        y = dat.propcoev$MinCoev,
        type = "scatter3d", mode = "markers")

# The probability of methods to overlap depends on the substitution rate.

# How many coevolving positions are detected by volume and charge?
x <- sets[,c("Volume", "Charge")]
sum(apply(x, 1, sum) > 0) * 100 / sum(apply(sets, 1, sum) > 0) #34.18%

# How many gene families have coevolving positions?
datc.f <- ddply(datc, .variables = "Family", .fun = dplyr::summarize, NbCoevolving = sum(IsCoevolving), NbSites = length(IsCoevolving))
datc.f$PropCoevolving <- with(datc.f, NbCoevolving / NbSites)
hist(datc.f$NbCoevolving)
hist(datc.f$PropCoevolving)
sum(datc.f$NbCoevolving!=0) * 100 / nrow(datc.f) #98.59% of family have at least 2 coevolving positions
summary(datc.f$NbCoevolving)
summary(datc.f$PropCoevolving)

require(lm.br)

m.nbcoe <- lm.br(NbCoevolving~NbSites, data = datc.f, type = "LL")
m.nbcoe$mle() # Breakpoint at 230AA
p.nbcoe <- ggplot(datc.f, aes(x = NbSites, y = NbCoevolving)) +
  geom_point() +
  geom_vline(xintercept = m.nbcoe$coef["theta"], col = "orange", size = 1) +
  geom_function(fun = Vectorize(function(x) {
    alpha <- m.nbcoe$coef["alpha"]
    theta <- m.nbcoe$coef["theta"]
    B <- m.nbcoe$coef["  NbSites < theta"]
    Bp <- m.nbcoe$coef["  NbSites > theta"]
    return(alpha + B * min(x - theta, 0) + Bp * max(x - theta, 0))
  }), col = "blue", size = 1) +
  ylim(0, 120) + xlab("Number of sites") + ylab("Number of coevolving sites") +
  theme_pubclean()
p.nbcoe
# The number of coevolving sites increases linearly with the protein length up to 230AA, and then decreases again.


m.propcoe <- lm.br(PropCoevolving~NbSites, data = datc.f, type = "TL")
m.propcoe$mle() # Breakpoint at 136AA
p.propcoe <- ggplot(datc.f, aes(x = NbSites, y = PropCoevolving)) +
  geom_point() +
  geom_vline(xintercept = m.propcoe$coef["theta"], col = "orange", size = 1) +
  geom_function(fun = Vectorize(function(x) {
    alpha <- m.propcoe$coef["alpha"]
    theta <- m.propcoe$coef["theta"]
    B <- m.propcoe$coef["  NbSites < theta"]
    Bp <- m.propcoe$coef["  NbSites > theta"]
    return(alpha + B * min(x - theta, 0) + Bp * max(x - theta, 0))
  }), col = "blue", size = 1) +
  xlab("Number of sites") + ylab("Proportion of coevolving sites") +
  scale_y_continuous(labels = scales::percent, limits = c(0, 0.6)) +
  theme_pubclean()
p.propcoe
# The proportion of coevolving sites is stable around 20% up to 300AA, and then decreases.



dat.fam <- read.table("FamilyStatistics.tsv", header = TRUE, stringsAsFactors = FALSE)
datc.f <- merge(datc.f, dat.fam, by = "Family")
# We use this data to create Supp Table S1:
stable <- read.csv("FamilyProperties.csv")
stable <- merge(stable, datc.f, by = c("Family", "NbSites"), all = TRUE)
write.csv(stable, file = "SupplementaryTable-FamilyProperties.csv")

m.nbcoe2 <- lm.br(NbCoevolving~SequenceMedianLength, data = datc.f, type = "LL")
m.nbcoe2$mle() # Breakpoint at 316AA
p.nbcoe2 <- ggplot(datc.f, aes(x = SequenceMedianLength, y = NbCoevolving)) +
  geom_point() +
  geom_vline(xintercept = m.nbcoe2$coef["theta"], col = "orange", size = 1) +
  geom_function(fun = Vectorize(function(x) {
    alpha <- m.nbcoe2$coef["alpha"]
    theta <- m.nbcoe2$coef["theta"]
    B <- m.nbcoe2$coef["  SequenceMedianLength < theta"]
    Bp <- m.nbcoe2$coef["  SequenceMedianLength > theta"]
    return(alpha + B * min(x - theta, 0) + Bp * max(x - theta, 0))
  }), col = "blue", size = 1) +
  ylim(0, 120) + xlab("Median sequence length") + ylab("Number of coevolving sites") +
  theme_pubclean()
p.nbcoe2
# Similar pattern than when using nb of sites

m.propcoe2 <- lm.br(PropCoevolving~SequenceMedianLength, data = datc.f, type = "TL")
m.propcoe2$mle() # Breakpoint at 149AA
p.propcoe2 <- ggplot(datc.f, aes(x = SequenceMedianLength, y = PropCoevolving)) +
  geom_point() +
  geom_vline(xintercept = m.propcoe2$coef["theta"], col = "orange", size = 1) +
  geom_function(fun = Vectorize(function(x) {
    alpha <- m.propcoe2$coef["alpha"]
    theta <- m.propcoe2$coef["theta"]
    B <- m.propcoe2$coef["  SequenceMedianLength < theta"]
    Bp <- m.propcoe2$coef["  SequenceMedianLength > theta"]
    return(alpha + B * min(x - theta, 0) + Bp * max(x - theta, 0))
  }), col = "blue", size = 1) +
  scale_y_continuous(labels = scales::percent, limits = c(0, 0.6)) +
  xlab("Median sequence length") + ylab("Proportion of coevolving sites") +
  theme_pubclean()
p.propcoe2


# What can be the reason?
p.lengthVsTreeLength <- ggplot(datc.f, aes(x = SequenceMedianLength, y = TreeTotalLength)) +
  geom_point() +
  geom_smooth(method = "lm", se = FALSE) +
  theme_pubclean() + xlab("Median sequence length") + ylab("Total tree length")
p.lengthVsTreeLength
cor.test(~TreeTotalLength+SequenceMedianLength, datc.f, method = "kendall")

p.lengthVsTreeDiameter <- ggplot(datc.f, aes(x = SequenceMedianLength, y = TreeDiameter)) +
  geom_point() +
  geom_smooth(method = "lm", se = FALSE) +
  scale_x_log10() +
  theme_pubclean() + xlab("Median sequence length") + ylab("Tree diameter")
p.lengthVsTreeDiameter
cor.test(~TreeDiameter+SequenceMedianLength, datc.f, method = "kendall")

# Similar trend: longer alignments are more conserved (smaller trees)

p.propcoeVsTreeDiameter <- ggplot(datc.f, aes(x = TreeDiameter, y = PropCoevolving)) +
  geom_point() +
  geom_smooth(method = "lm", se = FALSE) +
  scale_y_continuous(labels = scales::percent) +
  theme_pubclean() + xlab("Tree diameter") + ylab("Proportion of coevolving sites")
p.propcoeVsTreeDiameter
cor.test(~TreeDiameter+PropCoevolving, datc.f, method = "kendall")

p.propcoeVsTreeLength <- ggplot(datc.f, aes(x = TreeTotalLength, y = PropCoevolving)) +
  geom_point() +
  geom_smooth(method = "lm", se = FALSE) +
  scale_y_continuous(labels = scales::percent) +
  theme_pubclean() + xlab("Total tree length") + ylab("Proportion of coevolving sites")
p.propcoeVsTreeLength
cor.test(~TreeTotalLength+PropCoevolving, datc.f, method = "kendall")

# Figure S1
p.sfig <- plot_grid(p.nbcoe, p.lengthVsTreeDiameter, p.propcoeVsTreeDiameter, labels = "AUTO", nrow = 1)
p.sfig

ggsave2(p.sfig, filename = "CoevolvingSitesPerFamily.pdf", width = 12, height = 4)



#### Analysis of structure variables ####

### Compare measures of solvent accessibility and rates of protein evolution ###

dat.pca <- na.omit(datc[,c("Rsa", "ResidueDepth", "CalphaDepth", "NbContact5", "NbContact8", "NbContact10", paste("Np.", indices, sep = ""))])

require(ade4)
dudi <- dudi.pca(dat.pca, scannf = FALSE, nf = 12, center = TRUE, scale = TRUE)
s.corcircle(dudi$co)

p12 <- ggplot(dudi$co) +
  geom_segment(x=0, y=0, aes(xend = Comp1, yend = Comp2), arrow = arrow(angle = 10, length = unit(10, "pt"), type = "closed")) +
  annotate("path",
           x = cos(seq(0, 2*pi, length.out=100)),
           y = sin(seq(0, 2*pi, length.out=100))) +
  geom_label_repel(aes(x = Comp1, y = Comp2, label = rownames(dudi$co)), max.overlaps = 20) +
  xlim(-1,1) + ylim(-1,1) + theme_pubr() + xlab("1st component") + ylab("2nd component")
p12

p34 <- ggplot(dudi$co) +
  geom_segment(x=0, y=0, aes(xend = Comp3, yend = Comp4), arrow = arrow(angle = 10, length = unit(10, "pt"), type = "closed")) +
  annotate("path",
           x = cos(seq(0, 2*pi, length.out=100)),
           y = sin(seq(0, 2*pi, length.out=100))) +
  geom_label_repel(aes(x = Comp3, y = Comp4, label = rownames(dudi$co)), max.overlaps = 20) +
  xlim(-1,1) + ylim(-1,1) + theme_pubr() + xlab("3rd component") + ylab("4th component")
p34

#=> All RSA measures are very well (anti) correlated. We can use RSA only.
# RSA is somehow correlated with all rate measure, most particularly charge.

# Correlogram:
dat.cor <- cor(dat.pca, method = "spearman")

library(ggcorrplot)
p.cor <- ggcorrplot(dat.cor, hc.order = FALSE, type = "lower", lab = TRUE, lab_size = 3)
p.cor

require(cowplot)
p <- plot_grid(p12, p.cor, nrow = 1, labels = "AUTO")
p
ggsave(p, filename = "RsaCorrelations.pdf", width = 16, height = 8)

# Looking at RSA vs each rate:
datm.full <- read.table("AllFamilies_sites_molted.csv.gz", header = TRUE, sep = ",", stringsAsFactors = FALSE)
datm <- subset(datm.full, ! Method %in% c("vol3", "pol3"))
indices <- c("ind1", "ind2", "ind3", "ind4", "ind5", "ind6", "ind7", "ind8", "vol", "pol", "charge")
datm$Property <- factor(datm$Method, levels = indices, labels = c(paste("I", c(5:8, 1:4), sep = ""), "Volume", "Polarity", "Charge"))

require(Hmisc)
p.rsa.cor <- ggplot(datm[!is.na(datm$Rsa),], aes(y=Np, x=cut2(Rsa, g = 5, levels.mean = TRUE, formatfun = function(x) formatC(x, format = "f", digits = 3)))) + geom_boxplot() +
  scale_y_continuous(trans = "sqrt", breaks = c(0, 0.1, 0.5, 2, 5, 10), minor_breaks = NULL) +
  scale_x_discrete(guide = guide_axis(n.dodge = 2)) +
  facet_wrap(~Property, as.table = FALSE) + theme_pubclean() + xlab("RSA") + ylab("Standardized rate")
p.rsa.cor

# Structural properties are independent of the method:
datr <- subset(datm, Method == "charge") # For instance, identical for all methods

p.ctc.cor <- ggplot(datr[!is.na(datr$Rsa),], aes(y=NbContact8, x=cut2(Rsa, g = 5, levels.mean = TRUE, formatfun = function(x) formatC(x, format = "f", digits = 3)))) + geom_boxplot() +
  scale_y_continuous(position = "right") +
  theme_pubclean() + xlab("RSA") + ylab("Number of residues\nin contact") +
  theme(axis.title.x = element_blank(),
        axis.text.x = element_blank(),
        axis.ticks.x = element_blank())
p.ctc.cor

p <- ggdraw(p.rsa.cor) +
  theme(plot.margin=unit(c(0,2,0,0),"cm")) +
  annotation_custom(ggplotGrob(p.ctc.cor),
                    xmin = 0.765, xmax = 1.1,
                    ymin = 0.7, ymax = 0.93) +
  draw_label("A", 0, 1, hjust = 0, vjust = 1) +
  draw_label("B", 0.765, 1, hjust = 0, vjust = 1)
p

ggsave(p, filename = "RsaCorrelations2.pdf", width = 7, height = 7)

tests <- ddply(.data = datm, .var = "Method", .fun = function(d) {
    test <- cor.test(~Np+Rsa, d, method = "spearman", exact = FALSE)
    return(c("Correlation" = test$estimate, "P-value" = test$p.value))
  })
tests
# All positively and significantly correlated. Charge has the strongest correlation.


### Secondary structure and intrinsic disorder ###

require(ggplot2)
ggplot(datc, aes(x=COILS, y=REM465)) + geom_density_2d() + geom_quantile() #Poorly correlated
ggplot(datc, aes(x=COILS, y=HOTLOOP)) + geom_density_2d() + geom_quantile() #Strongly correlated
ggplot(datc, aes(x=REM465, y=HOTLOOP)) + geom_density_2d() + geom_quantile() #Strongly correlated
# We use the HOTLOOP variable as an indicator of disorder.

table(datc$SecondaryStructure)
datc$SecondaryStructure[datc$SecondaryStructure == " "] <- NA

datc.rsa <- na.omit(datc[,c("SecondaryStructure", "Rsa")])
p <- ggboxplot(datc.rsa, x = "SecondaryStructure", y = "Rsa", notch = TRUE, varwidth = TRUE)
p

# Significance groups:
require(agricolae)
kruskal(datc.rsa$Rsa, datc.rsa$SecondaryStructure, group = TRUE, console = TRUE)
# All different!

datc.dis <- na.omit(datc[,c("SecondaryStructure", "HOTLOOP")])
p <- ggboxplot(datc.dis, x = "SecondaryStructure", y = "HOTLOOP", notch = TRUE, varwidth = TRUE) +
  geom_hline(yintercept=0.1204) #threshold for considering a region as disordered
p

# Significance groups:
kruskal(datc.dis$HOTLOOP, datc.dis$SecondaryStructure, group = TRUE, console = TRUE)
# Regions without structure are more disordered, but pattern is not as strong as for RSA

cor.test(~Rsa+HOTLOOP, datc, method =  "spearman")
cor.test(~Np.charge+HOTLOOP, datc, method =  "spearman")
cor.test(~Np.pol+HOTLOOP, datc, method =  "spearman")
cor.test(~Np.vol+HOTLOOP, datc, method =  "spearman")
# All positive and significant

# Assign disordered regions:
with(datc, table(HOTLOOP >= 0.1204, datc$SecondaryStructure))
datc$IsDisordered <- datc$HOTLOOP > 0.1204 & !datc$SecondaryStructure %in% c("H", "G", "E")




#### Analyses of individual properties ####

datm.full <- read.table("AllFamilies_sites_molted.csv.gz", header = TRUE, sep = ",", stringsAsFactors = FALSE)
datm.full$IsCoevolving <- !is.na(datm.full$Size)
datm.full$IsCoevolvingDisplay <- ifelse(!is.na(datm.full$Size), "yes", "no")
datm.full$IsDisordered <- datm.full$HOTLOOP > 0.1204 & !datm.full$SecondaryStructure %in% c("H", "G", "E")
datm.full$SecondaryStructure[datm.full$SecondaryStructure == " "] <- NA
datm.full$SecondaryStructure[datm.full$IsDisordered] <- "Dis"
datm.full$SecondaryStructure <- factor(datm.full$SecondaryStructure, levels = c("-", "H", "G", "I", "E", "B", "T", "S", "Dis"),
                                  labels = c("None", sprintf("\u03b1-helix"), "3-10 helix", sprintf("\u03c0-helix"), "Strand", sprintf("\u03b2-bridge"), "Turn", "Bend", "Disordered"))
# Note: we set "None" as the reference level in contrasts
datm <- subset(datm.full, ! Method %in% c("vol3", "pol3"))
indices <- c("ind1", "ind2", "ind3", "ind4", "ind5", "ind6", "ind7", "ind8", "vol", "pol", "charge")
datm$Property <- factor(datm$Method, levels = indices, labels = c(paste("I", c(5:8, 1:4), sep = ""), "Volume", "Polarity", "Charge"))

tbl <- table(datm$Method, datm$IsCoevolving)
print(tbl)
plot(tbl)
chisq.test(tbl)
# There are more coevolving positions found with traditional measures (Volume, Polarity, Charge)

# Secondary structure and evolutionary rate:
p <- ggboxplot(datm, x = "SecondaryStructure", y = "Np", notch = TRUE, varwidth = TRUE, outlier.shape = 1) +
  scale_y_sqrt() + #TODO define ticks manually
  facet_wrap(~Property, strip.position = "top") +
  coord_flip() +
  scale_x_discrete(limits = c(NA, rev(levels(datm$SecondaryStructure)))) +
  xlab("Secondary structure") + ylab("Rate of change")
p

kt.groups <- ddply(datm, .variables = "Property", .fun = function(d) {
  m <- unique(d$Method)
  print(m)
  t <- kruskal(d$Np, d$SecondaryStructure, group = TRUE, alpha = 0.01)
  g <- t$groups
  g$SecondaryStructure <- rownames(g)
  g$Method <- m
  return(g)
})

kt.groups$SecondaryStructure[kt.groups$SecondaryStructure == "NA"] <- NA

p + geom_text(data = kt.groups, aes(x = SecondaryStructure, y = 15, label = groups), hjust =  "right")


# Impact of evolutionary rate on coevolution:
datm.sum <- ddply(datm, .variables = c("IsCoevolvingDisplay", "Property"),
                  .fun = plyr::summarize, Mean = mean(Np))
datm.sum <- ddply(datm.sum, .variables = "Property", .fun = function(dat) {
  d <- diff(dat$Mean)
  d <- 2*sign(d) * d*d
  print(d)
  dat$Label.x <- c(-0.5, 3.5)
  dat$Label.y <- dat$Mean + c(-d, d)
  dat$Label.y[dat$Label.y < 0] <- 0
  return(dat)
})

p.rate <- ggplot(datm, aes(x = IsCoevolvingDisplay, y = Np)) +
  geom_boxplot() +
  geom_segment(data = datm.sum, aes(x = IsCoevolvingDisplay, y = Mean, xend = Label.x, yend = Label.y), size = 0.15) +
  geom_point(data = datm.sum, aes(x = IsCoevolvingDisplay, y = Mean), shape = 23, size = 2, fill = "white") +
  geom_label(data = datm.sum, aes(x = Label.x, y = Label.y, label = sprintf("%.2f", Mean)),
             size = 2, label.size = 0.15) +
  facet_wrap(~Property, as.table = FALSE) +
  scale_y_sqrt(breaks = c(0,1,2,5,10,15,20), limits = c(0,20)) +
  scale_x_discrete(expand = c(1,1)) +
  xlab("Site is coevolving") + ylab("Standardized rate") +
  theme_pubr()

comp <- list(c("yes", "no"))
p.rate <- p.rate + stat_compare_means(comparisons = comp, method = "wilcox.test", aes(label = as_label(..p.signif..)))
p.rate
ggsave(p.rate, filename = "RateVsCoevolution.pdf", width = 7, height = 7)


# => For all properties, coevolving sites are slower. But sites coevolving for charge are faster!
# Is this an effect of continuous vs. discrete variables? We look at discretized volume and polarity:
datm.d <- subset(datm.full, Method %in% c("vol", "pol", "vol3", "pol3"))
indices.d <- c("vol", "pol", "vol3", "pol3")
datm.d$Property <- factor(datm.d$Method, levels = indices.d, labels = c("Volume", "Polarity", "Discretized volume", "Discretized polarity"))

datm.d.sum <- ddply(datm.d, .variables = c("IsCoevolvingDisplay", "Property"),
                  .fun = plyr::summarize, Mean = mean(Np))
datm.d.sum <- ddply(datm.d.sum, .variables = "Property", .fun = function(dat) {
  d <- diff(dat$Mean)
  d <- 2*sign(d) * d*d
  print(d)
  dat$Label.x <- c(-0.5, 3.5)
  dat$Label.y <- dat$Mean + c(-d, d)
  dat$Label.y[dat$Label.y < 0] <- 0
  return(dat)
})

p.rate.d <- ggplot(datm.d, aes(x = IsCoevolvingDisplay, y = Np)) +
  geom_boxplot() +
  geom_segment(data = datm.d.sum, aes(x = IsCoevolvingDisplay, y = Mean, xend = Label.x, yend = Label.y), size = 0.15) +
  geom_point(data = datm.d.sum, aes(x = IsCoevolvingDisplay, y = Mean), shape = 23, size = 2, fill = "white") +
  geom_label(data = datm.d.sum, aes(x = Label.x, y = Label.y, label = sprintf("%.2f", Mean)),
             size = 2, label.size = 0.15) +
  facet_wrap(~Property, as.table = FALSE) +
  scale_y_sqrt(breaks = c(0,1,2,5,10,15,20), limits = c(0,20)) +
  scale_x_discrete(expand = c(1,1)) +
  xlab("Site is coevolving") + ylab("Standardized rate") +
  theme_pubr()

comp <- list(c("yes", "no"))
p.rate.d <- p.rate.d + stat_compare_means(comparisons = comp, method = "wilcox.test", aes(label = as_label(..p.signif..)))
p.rate.d
ggsave(p.rate.d, filename = "RateVsCoevolutionDiscretized.pdf", width = 5, height = 5.5) # Figure S2
# => Very consistent results, this is not an effect of the discrete nature of the charge variable.

# Impact of RSA on coevolution:
datm.sum2 <- ddply(datm, .variables = c("IsCoevolvingDisplay", "Property"),
                  .fun = plyr::summarize, Mean = mean(Rsa, na.rm = TRUE))
datm.sum2 <- ddply(datm.sum2, .variables = "Property", .fun = function(dat) {
  d <- diff(dat$Mean)
  d <- 2*sign(d) * d*d
  print(d)
  dat$Label.x <- c(-0.5, 3.5)
  dat$Label.y <- dat$Mean + c(-d, d)
  dat$Label.y[dat$Label.y < 0] <- 0
  return(dat)
})

p.rsa <- ggplot(datm, aes(x = IsCoevolvingDisplay, y = Rsa)) +
  geom_boxplot() +
  geom_segment(data = datm.sum2, aes(x = IsCoevolvingDisplay, y = Mean, xend = Label.x, yend = Label.y), size = 0.15) +
  geom_point(data = datm.sum2, aes(x = IsCoevolvingDisplay, y = Mean), shape = 23, size = 2, fill = "white") +
  geom_label(data = datm.sum2, aes(x = Label.x, y = Label.y, label = sprintf("%.2f", Mean)),
             size = 2, label.size = 0.15) +
  facet_wrap(~Property, as.table = FALSE) +
  scale_y_sqrt(breaks = c(0, 0.1, 0.3, 0.5, 1.0), limits = c(0,1.4)) +
  scale_x_discrete(expand = c(1,1)) +
  xlab("Site is coevolving") + ylab("RSA") +
  theme_pubr()

comp <- list(c("yes", "no"))
p.rsa <- p.rsa + stat_compare_means(comparisons = comp, method = "wilcox.test", aes(label = as_label(..p.signif..)))
p.rsa

# Coevolving residues are more buried, with the exception of residues coevolving for charge.

# How rare are compensatory mutations?
datm.full.rank <- ddply(.data=datm.full, .variables = c("Method", "Family"), .fun = plyr::summarize,
                        NminRank = rank(Nmin), IsCoevolvingDisplay = IsCoevolvingDisplay)

ddply(.data = datm.full.rank, .variables = "IsCoevolvingDisplay", .fun = plyr::summarize, MedianRank = median(NminRank))
#IsCoevolvingDisplay MedianRank
#1                  no   125.0
#2                 yes     4.5

#### GLMM ####

library(lme4)
fit.glmer <- function(dat, property) {
  dat.ppt <- na.omit(subset(dat, Method == property, select = c("IsCoevolving", "Np", "Rsa", "SecondaryStructure", "Family")))
  gm <- glmer(IsCoevolving ~ Np + Rsa*SecondaryStructure + (1|Family),
              data = dat.ppt, family = "binomial", verbose = 1)
  return(gm)
}
fit.glmer(datm, "charge")
# Model fails to converge. We try some other estimation approaches.

library(MASS)
library(glmmsr)

get.p.values <- function(m) {
  if ("lme" %in% class(m)) {
    return(as.data.frame(summary(m)$tTable)$`p-value`)
  } else if ("glmmFit" %in% class(m)) {
    return(summary(m)$p_value[-1]) #First term is the random effect
  } else return(NULL)
}

get.effects <- function(m) {
  if ("lme" %in% class(m)) {
    return(m$coefficients$fixed)
  } else if ("glmmFit" %in% class(m)) {
    return(m$estim[-1]) #First term is the random effect
  } else return(NULL)
}

fit.glmm <- function(dat, property, methods = c("pql", "lap", "agq", "sr", "is"), niter = 100, verbose = TRUE) {
  dat.ppt <- na.omit(subset(dat, Method == property, select = c("IsCoevolving", "Np", "Rsa", "SecondaryStructure", "Family")))
  results <- list()
  for (m in methods) {
    model <- NULL
    if (m == "pql") {
      cat("Fitting model with PQL method...\n")
      require(MASS)
      try(model <- glmmPQL(IsCoevolving ~ Np + Rsa*SecondaryStructure, random = ~ 1|Family,
                           data = dat.ppt, family = "binomial", niter = niter, verbose = verbose))
    } else if (m == "lap") {
      require(glmmsr)
      cat("Fitting model with Laplace method...\n")
      try(model <- glmm(IsCoevolving ~ Np + Rsa*SecondaryStructure + (1|Family),
                        data = dat.ppt, family = "binomial", method = "Laplace"))
    } else if (m == "agq") {
      require(glmmsr)
      cat("Fitting model with AGQ method...\n")
      try(model <- glmm(IsCoevolving ~ Np + Rsa*SecondaryStructure + (1|Family),
                        data = dat.ppt, family = "binomial", method = "AGQ"))
    } else if (m == "sr") {
      require(glmmsr)
      cat("Fitting model with SR method...\n")
      try(model <- glmm(IsCoevolving ~ Np + Rsa*SecondaryStructure + (1|Family),
                        data = dat.ppt, family = "binomial", method = "SR"))
    } else if (m == "is") {
      require(glmmsr)
      cat("Fitting model with IS method...\n")
      try(model <- glmm(IsCoevolving ~ Np + Rsa*SecondaryStructure + (1|Family),
                        data = dat.ppt, family = "binomial", method = "IS"))
    }
    results[[m]] <- model
  }
  return(results)
}

models.charge <- fit.glmm(datm, "charge")
save(models.charge, file = "modelsCharge.Rdata")
#load("modelsCharge.Rdata")

#IS fails
plot(as.data.frame(lapply(models.charge[c("pql", "lap", "agq", "sr")], get.p.values)))
plot(as.data.frame(lapply(models.charge[c("pql", "lap", "agq", "sr")], get.effects)))
# The three methods give the same results. Note that for PQL the default value of niter = 10 does not allow to compute pvalues.

summary(models.charge$lap)
summary(models.charge$pql)

# Polarity
models.polarity <- fit.glmm(datm, "pol")
save(models.polarity, file = "modelsPolarity.Rdata")
#load("modelsPolarity.Rdata")

#IS fails
plot(as.data.frame(lapply(models.polarity[c("pql", "lap", "agq", "sr")], get.p.values)))
plot(as.data.frame(lapply(models.polarity[c("pql", "lap", "agq", "sr")], get.effects)))

# Volume
models.volume <- fit.glmm(datm, "vol")
save(models.volume, file = "modelsVolume.Rdata")
#load("modelsVolume.Rdata")

#IS fails
plot(as.data.frame(lapply(models.volume[c("pql", "lap", "agq", "sr")], get.p.values)))
plot(as.data.frame(lapply(models.volume[c("pql", "lap", "agq", "sr")], get.effects)))

# Running other indices, we only use PQL and Laplace, as the others are identical to Laplace
models.ind1 <- fit.glmm(datm, "ind1", methods = c("pql", "lap"))
models.ind2 <- fit.glmm(datm, "ind2", methods = c("pql", "lap"))
models.ind3 <- fit.glmm(datm, "ind3", methods = c("pql", "lap"))
models.ind4 <- fit.glmm(datm, "ind4", methods = c("pql", "lap"))
models.ind5 <- fit.glmm(datm, "ind5", methods = c("pql", "lap"))
models.ind6 <- fit.glmm(datm, "ind6", methods = c("pql", "lap"))
models.ind7 <- fit.glmm(datm, "ind7", methods = c("pql", "lap"))
models.ind8 <- fit.glmm(datm, "ind8", methods = c("pql", "lap")) #PQL does not converge even after 100 iterations!

save(models.ind1, file = "modelsIndice1.Rdata")
save(models.ind2, file = "modelsIndice2.Rdata")
save(models.ind3, file = "modelsIndice3.Rdata")
save(models.ind4, file = "modelsIndice4.Rdata")
save(models.ind5, file = "modelsIndice5.Rdata")
save(models.ind6, file = "modelsIndice6.Rdata")
save(models.ind7, file = "modelsIndice7.Rdata")
save(models.ind8, file = "modelsIndice8.Rdata")

#for (i in 1:8) {
#  print(i)
#  load(paste("modelsIndice", i, ".Rdata", sep = ""))
#}

# The PQL p-values are a bit lower, and ind8 has difficulties in converging. We will then report the Laplace estimates, but use PQL for computing VIF.

## Look at VIF

get.vif <- function(m, method) {
  require(car)
  d <- as.data.frame(vif(m)[,3])
  names(d) <- method
  d$variable <-row.names(d)
  return(d)
}

tbl.cha <- get.vif(models.charge$pql, "charge")
tbl.pol <- get.vif(models.polarity$pql, "polarity")
tbl.vol <- get.vif(models.volume$pql, "volume")

tbl <- tbl.vol
tbl <- merge(tbl, tbl.pol, by = "variable", all = TRUE, sort = FALSE)
tbl <- merge(tbl, tbl.cha, by = "variable", all = TRUE, sort = FALSE)
tbl

tbl2.lst <- list()
for (i in 1:8) {
  tbl2.lst[[i]] <- get.vif(get(paste("models.ind", i, sep = ""))$pql, paste("ind", i, sep = ""))
}
tbl2 <- tbl2.lst[[1]]
for (i in 2:8) {
  tbl2 <- merge(tbl2, tbl2.lst[[i]], by = "variable", all = TRUE, sort = FALSE)
}


tbl3 <- merge(tbl, tbl2, by = "variable", all = TRUE, sort = FALSE)

require(xtable)
tab3 <- xtable(tbl3, align = rep("r", 13), )
print(tab3, include.rownames = FALSE, type = "html", file = "GlmmVif.html")


## Report estimates:

# From gtools:
stars.pval <- function(p.value) {
  unclass(
    symnum(p.value, corr = FALSE, na = FALSE,
           cutpoints = c(0, 0.001, 0.01, 0.05, 0.1, 1),
           symbols = c("(***)", "(**)", "(*)", "(.)", ""))
  )
}

get.signif.effects <- function(m, threshold, method) {
  p <- get.p.values(m)
  f <- get.effects(m)
  d <- data.frame(effect = f, p.value = p)
  d <- subset(d, p.value <= threshold)
  d$p.signif <- stars.pval(d$p.value)
  d$variable <- row.names(d)
  d[, method] <- paste(round(d$effect*100)/100, " ", d$p.signif, sep = "")
  return(d)
}

tbl.cha <- get.signif.effects(models.charge$lap, 1, "charge")
tbl.pol <- get.signif.effects(models.polarity$lap, 1, "polarity")
tbl.vol <- get.signif.effects(models.volume$lap, 1, "volume")

tbl <- tbl.vol[,c("variable", "volume")]
tbl <- merge(tbl, tbl.pol[, c("variable", "polarity")], by = "variable", all = TRUE, sort = FALSE)
tbl <- merge(tbl, tbl.cha[, c("variable", "charge")], by = "variable", all = TRUE, sort = FALSE)
tbl

tbl2.lst <- list()
for (i in 1:8) {
  tbl2.lst[[i]] <- get.signif.effects(get(paste("models.ind", i, sep = ""))$lap, 1, paste("ind", i, sep = ""))
}
tbl2 <- tbl2.lst[[1]][, c("variable", "ind1")]
for (i in 2:8) {
  tbl2 <- merge(tbl2, tbl2.lst[[i]][, c("variable", paste("ind", i, sep = ""))], by = "variable", all = TRUE, sort = FALSE)
}


tbl3 <- merge(tbl, tbl2, by = "variable", all = TRUE, sort = FALSE)

require(xtable)
tab <- xtable(tbl, align = rep("r", 5), )
print(tab, include.rownames = FALSE)

tab2 <- xtable(tbl2, align = rep("r", 10), )
print(tab2, include.rownames = FALSE)

tab3 <- xtable(tbl3, align = rep("r", 13), )
print(tab3, include.rownames = FALSE, type = "html", file = "GlmmResults.html")

# Test for indice 3 and disordered regions

require(seqinr)
data(aaindex)
index <- aaindex$MAXF760101$I
names(index) <- toupper(names(index))
datc$AA <- sapply(strsplit(datc$PDB, split ="[\\[\\]]", perl = TRUE), function(x) substring(x[2], 1, 3))
datc$AlphaAndTurnPropensity <- index[datc$AA]

datc$SecondaryStructure[datc$IsDisordered] <- "Dis"
datc$SecondaryStructure <- factor(datc$SecondaryStructure, levels = c("-", "H", "G", "I", "E", "B", "T", "S", "Dis"),
                                       labels = c("None", sprintf("\u03b1-helix"), "3-10 helix", sprintf("\u03c0-helix"), "Strand", sprintf("\u03b2-bridge"), "Turn", "Bend", "Disordered"))

boxplot(AlphaAndTurnPropensity~SecondaryStructure, datc)
