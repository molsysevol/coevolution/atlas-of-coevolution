# Created on 10/03/20 by jdutheil

library(dplyr)

fams <- read.table("Families/Bacteria/final_fams_complete.txt")

read.sged <- function(family, method) {
  file <- paste("Results/Bacteria/", family, "/", family, "_structureSitesPairs_infos_", method, ".tsv", sep = "")
  dat <- read.table(file, header = TRUE, sep = "\t", stringsAsFactors = FALSE)
  dat <- dat[,c("PDB", "RsaMin", "RsaMax", "SecondaryStructure", "AlphaDistMean", "IsCoevolving")]
  names(dat)[6] <- paste("IsCoevolving", method, sep = ".")
  dat$Family <- family
  ###
  return(dat)
}

read.allfams <- function() {
  require(data.table)

  pb <- txtProgressBar(0, nrow(fams), style = 3)
  dat.lst <- list()
  i <- 0
  for (f in fams$V1) {
    dat <- read.sged(f, "ind1")
    for (m in c("ind2", "ind3", "ind4", "ind5", "ind6", "ind7", "ind8", "vol", "pol", "charge")) {
      dat2 <- read.sged(f, m)
      #Slow!
      #dat <- merge(dat, dat2, by = c("Family", "PDB", "RsaMin", "RsaMax", "SecondaryStructure", "AlphaDistMean"), all = TRUE)
      #Use dplyr instead, since the exact same groups are presents in all methods
      dat <- inner_join(dat, dat2, by = c("Family", "PDB", "RsaMin", "RsaMax", "SecondaryStructure", "AlphaDistMean"))
    }
    i <- i + 1
    setTxtProgressBar(pb, i)
    dat.lst[[f]] <- dat
  }
  dat <- rbindlist(dat.lst)
  return(dat)
}

dat <- read.allfams()
write.csv(dat, file = paste("Results/Bacteria/StructureSitePairsAllFamilies_sites_casted.csv", sep = ""), row.names = FALSE)

