//Created on 30/07/20 by jdutheil
// 
// This script performs:
// 1) read the filtered alignment in mase format
// 2) retrieve the site selection
// 3) look for the longest sequence withou gap and N in the filtered alignment
// 4) extract the original sequence
// 5) unalign sequence (remove gaps) and export to fasta
//
// Compile with: c++ -std=c++11 -L$HOME/.local/lib -I$HOME/.local/include get-longest-seq.cpp -lbpp-seq -lbpp-core -o get-longest-seq
//
// Example usage:
// Scripts/get-longest-seq input.sequence.file=Families/Bacteria/HOG000004796/HOG000004796_bppalnscore.mase site.selection=CS output.sequence.file=Disorder/HOG000004796_sequence.fasta




// From the STL:
#include <iostream>
#include <iomanip>
#include <map>
#include <string>
using namespace std;

// From Bio++
#include <Bpp/App/BppApplication.h>
#include <Bpp/App/ApplicationTools.h>
#include <Bpp/Seq/Alphabet/AlphabetTools.h>
#include <Bpp/Seq/Io/Mase.h>
#include <Bpp/Seq/Io/Fasta.h>
#include <Bpp/Seq/Io/MaseTools.h>
#include <Bpp/Seq/Container/VectorSiteContainer.h>
#include <Bpp/Seq/SequenceTools.h>
using namespace bpp;

int main(int args, char** argv)
{
  try
  {
    BppApplication app(args, argv, "get-longest-seq");

    //Read the alignment
    string sequenceFilePath = ApplicationTools::getAFilePath("input.sequence.file", app.getParams(), true, true, "", true, "none", 1); 
    Mase maseReader;
    VectorSiteContainer sites(&AlphabetTools::PROTEIN_ALPHABET);
    maseReader.readAlignment(sequenceFilePath, sites);

    //Retrieve the selected sites:
    string siteSet = ApplicationTools::getStringParameter("site.selection", app.getParams(), "none", "", true, 1);
    unique_ptr<VectorSiteContainer> selectedSites;
    if (siteSet != "none")
    {
      try
      {
        selectedSites.reset(dynamic_cast<VectorSiteContainer*>(MaseTools::getSelectedSites(sites, siteSet)));
        ApplicationTools::displayResult("Set found", TextTools::toString(siteSet) + " sites.");
      }
      catch (IOException& ioe)
      {
        throw ioe;
      }
      if (selectedSites->getNumberOfSites() == 0)
      {
        throw Exception("Site set '" + siteSet + "' is empty.");
      }
    } else {
       throw Exception("A site set should be provided.");
    }

    // Now we look for the longest sequence is the selected set:
    size_t nbSites, maxNbSites = 0;
    string longestSeqName = "";
    for (size_t i = 0; i < selectedSites->getNumberOfSequences(); ++i) {
      nbSites = SequenceTools::getNumberOfCompleteSites(selectedSites->getSequence(i));
      if (nbSites > maxNbSites) {
        maxNbSites = nbSites;
        longestSeqName = selectedSites->getSequence(i).getName();
      }
    }
    ApplicationTools::displayResult("Longuest resolved sequence is", longestSeqName);

    //Extract the original sequence:
    AlignedSequenceContainer selSeq(&AlphabetTools::PROTEIN_ALPHABET);
    vector<string> sel;
    sel.push_back(longestSeqName);
    SequenceContainerTools::getSelectedSequences(sites, sel, selSeq);
    SiteContainerTools::removeGapOnlySites(selSeq);

    //Finally, write the selected sequence to a Fasta file:
    Fasta fastaWriter;
    string outputFilePath = ApplicationTools::getAFilePath("output.sequence.file", app.getParams(), true, false, "", true, "none", 1); 
    fastaWriter.writeAlignment(outputFilePath, selSeq, true);
   
    cout << "Done!" << endl;
  }
  catch(exception& e)
  {
    cout << e.what() << endl;
    return 1;
  }

  return 0;

}


