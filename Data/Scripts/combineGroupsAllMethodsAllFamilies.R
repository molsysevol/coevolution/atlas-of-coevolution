# Created on 11/03/20 by jdutheil

require(data.table)

fams <- read.table("Families/Bacteria/final_fams_complete.txt")

read.sged <- function(family, method) {
  file <- paste("Results/Bacteria/", family, "/", family, "_", method, "_significant_PDB_infos2.tsv", sep = "")
  if (! file.exists(file)) {
    cat("Secondary structure labels not available for family", family, "\n")
    file <- paste("Results/Bacteria/", family, "/", family, "_", method, "_significant_PDB_infos.tsv", sep = "")
  }
  dat <- read.table(file, header = TRUE, sep = "\t", stringsAsFactors = FALSE, quote = "\"")
  if (nrow(dat) > 0) {
    dat$Family <- family
    dat$Method <- method
    return(dat)
  } else {
    return(NULL)
  }
}

read.allfams <- function(method) {
  pb <- txtProgressBar(0, nrow(fams), style = 3)
  dat.lst <- list()
  i <- 0
  for (f in fams$V1) {
    i <- i + 1
    setTxtProgressBar(pb, i)
    dat.lst[[f]] <- read.sged(f, method)
  }
  dat <- rbindlist(dat.lst, fill = TRUE)
  return(dat)
}

dat.lst <- list()
for (m in c("ind1", "ind2", "ind3", "ind4", "ind5", "ind6", "ind7", "ind8", "vol", "pol", "charge", "vol3", "pol3")) {
  dat.lst[[m]] <- read.allfams(m)
}
dat <- rbindlist(dat.lst)

write.csv(dat, file = paste("Results/Bacteria/AllFamilies_groups_molted.csv", sep = ""), row.names = FALSE)

