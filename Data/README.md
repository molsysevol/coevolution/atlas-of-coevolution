This directory allows to regenerate the big result tables used in the main directory.
The original data (sequence alignments, phylogenetic trees and CoMap results) is hosted separately on FigShare https://doi.org/10.6084/m9.figshare.16586915

After downloading, the data should be uncompressed in this directory:
```bash
cat Families-nonull-070921.tgz.?? > Families-nonull-070921.tgz
md5sum -c Families-nonull-070921.tgz.md5sum
tar xvzf Families-nonull-070921.tgz
```

All scripts and programs are in the `Scripts` subdirectory. The `get-longest-seq` program can be compiled with the command
```bash
c++ -std=c++11 \
    -L$HOME/.local/lib \
    -I$HOME/.local/include get-longest-seq.cpp \
    -lbpp-seq -lbpp-core \
    -o get-longest-seq
```
Assuming that the Bio++ libraries are installed in `$HOME/.local`.


Prepare dataset
===============

## Get raw data

## We create a PDB index for each family:

```bash
#If we are rerunning, first need to clean:
rm Families/Bacteria/*/*model0*

rm -f cmd-create-structure-index.sh

while IFS="" read -r p || [ -n "$p" ]
do
  echo "python Scripts/sged-create-structure-index.py \
         --pdb=Families/Bacteria/$p/*.pdb \
         --exclude-incomplete \
         --alignment=Families/Bacteria/$p/${p}_bppalnscore.mase \
         --output=Results/Bacteria/$p/${p}_StructureIndex.txt >& Results/Bacteria/$p/${p}_StructureIndex.log" >> cmd-create-structure-index.sh
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-create-structure-index.sh

```

Check that everything went ok:
```bash
grep Error Results/Bacteria/HOG000*/*StructureIndex.log
grep error Results/Bacteria/HOG000*/*StructureIndex.log
```

## We add the structural information for each site:

```bash
rm -f cmd-translate-site-coords.sh

while IFS="" read -r p || [ -n "$p" ]
do
  for method in ind1 ind2 ind3 ind4 ind5 ind6 ind7 ind8 vol pol charge
  do
    echo "python Scripts/sged-translate-coords.py \
           --sged=Families/Bacteria/$p/${p}_${method}_siteInfos.csv \
           --index=Results/Bacteria/$p/${p}_StructureIndex.txt \
           --output=Results/Bacteria/$p/${p}_${method}_siteInfos_PDB.tsv \
           --name=PDB >& Results/Bacteria/$p/${p}_${method}_TranslateCoords.log" >> cmd-translate-site-coords.sh
  done
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-translate-site-coords.sh

```
Check that everything went ok:
```bash
grep Error Results/Bacteria/HOG000*/*TranslateCoords.log
grep error Results/Bacteria/HOG000*/*TranslateCoords.log
```

### We do the same for discretized indices:

```bash
rm -f cmd-translate-site-coords-discrete.sh

while IFS="" read -r p || [ -n "$p" ]
do
  for method in vol3 pol3
  do
    echo "python Scripts/sged-translate-coords.py \
           --sged=Families/Bacteria/$p/${p}_${method}_siteInfos.csv \
           --index=Results/Bacteria/$p/${p}_StructureIndex.txt \
           --output=Results/Bacteria/$p/${p}_${method}_siteInfos_PDB.tsv \
           --name=PDB >& Results/Bacteria/$p/${p}_${method}_TranslateCoordsDiscrete.log" >> cmd-translate-site-coords-discrete.sh
  done
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-translate-site-coords-discrete.sh

```
Check that everything went ok:
```bash
grep Error Results/Bacteria/HOG000*/*TranslateCoordsDiscrete.log
grep error Results/Bacteria/HOG000*/*TranslateCoordsDiscrete.log
```


## We compute compute the structural information for each analysed site:

```bash
rm -f cmd-structure-list.sh

while IFS="" read -r p || [ -n "$p" ]
do
  # Get the best PDB structure used for the index:
  PDBFILE=`cat Results/Bacteria/$p/${p}_StructureIndex.txt | grep "# SGED input PDB = " | sed "s/# SGED input PDB = \(.*\)/\1/"`
  PDBCHAIN=`cat Results/Bacteria/$p/${p}_StructureIndex.txt | grep "# SGED input PDB chain = " | sed "s/# SGED input PDB chain = \(.*\)/\1/"`

  echo "python Scripts/sged-structure-list.py \
       --pdb=$PDBFILE \
       --chain=$PDBCHAIN \
       --output=Results/Bacteria/$p/${p}_structureSites.tsv >& Results/Bacteria/$p/${p}_StructureList.log" >> cmd-structure-list.sh
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-structure-list.sh
```

Check that everything went ok:
```bash
grep Error Results/Bacteria/HOG000*/*StructureList.log
grep error Results/Bacteria/HOG000*/*StructureList.log
```

### Compute structural informations for all sites

```bash
rm -f cmd-structure-infos.sh

while IFS="" read -r p || [ -n "$p" ]
do
  rm -f Families/Bacteria/$p/*_model0*
  # Get the best PDB structure used for the index:
  PDBFILE=`cat Results/Bacteria/$p/${p}_StructureIndex.txt | grep "# SGED input PDB = " | sed "s/# SGED input PDB = \(.*\)/\1/"`
  PDBCHAIN=`cat Results/Bacteria/$p/${p}_StructureIndex.txt | grep "# SGED input PDB chain = " | sed "s/# SGED input PDB chain = \(.*\)/\1/"`

  echo "python Scripts/sged-structure-infos.py \
       --sged=Results/Bacteria/$p/${p}_structureSites.tsv \
       --group=PDB \
       --pdb=$PDBFILE \
       --chain=$PDBCHAIN \
       --measures=DSSP,ContactMap,ResidueDepth \
       --output=Results/Bacteria/$p/${p}_structureSites_infos.tsv >& Results/Bacteria/$p/${p}_StructureInfos.log" >> cmd-structure-infos.sh
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-structure-infos.sh

```

Check that everything went ok:
```bash
grep Error Results/Bacteria/HOG000*/*StructureInfos.log
grep error Results/Bacteria/HOG000*/*StructureInfos.log
grep ERROR Results/Bacteria/HOG000*/*StructureInfos.log
```
=> 3 where DSSP could not be run:
HOG000035157
HOG000219148
HOG000231585
(all ribosomal proteins, only Ca available)

Add secondary structure labelling (needed for randomization)
  :
```bash
rm -f cmd-structure-infos2.sh

while IFS="" read -r p || [ -n "$p" ]
do
  rm -f Families/Bacteria/$p/*_model0*
  # Get the best PDB structure used for the index:
  PDBID=`cat Results/Bacteria/$p/${p}_StructureIndex.txt | grep "# SGED input PDB = " | sed "s/# SGED input PDB = \(.*\).pdb/\1/"`
  PDBCHAIN=`cat Results/Bacteria/$p/${p}_StructureIndex.txt | grep "# SGED input PDB chain = " | sed "s/# SGED input PDB chain = \(.*\)/\1/"`

  echo "python3 Scripts/sged-structure-infos.py \
       --sged=Results/Bacteria/$p/${p}_structureSites_infos.tsv \
       --group=PDB \
       --pdb=${PDBID}.cif \
       --pdb-format=mmCIF \
       --chain=$PDBCHAIN \
       --measures=SecondaryStructureLabel \
       --output=Results/Bacteria/$p/${p}_structureSites_infos2.tsv >& Results/Bacteria/$p/${p}_StructureInfos2.log" >> cmd-structure-infos2.sh
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-structure-infos2.sh

```


### Annotate intrinsically disordered regions

First, extract a representative sequence for each family. We take the one with most complete sites in the site selection:
```bash
rm -f cmd-get-longest-seq.sh

while IFS="" read -r p || [ -n "$p" ]
do
  mkdir Results/Bacteria/$p/DisEMBL
  echo "Scripts/get-longest-seq \
    input.sequence.file=Families/Bacteria/$p/${p}_bppalnscore.mase \
    site.selection=SPS \
    output.sequence.file=Results/Bacteria/$p/DisEMBL/${p}_sequence.fasta" >> cmd-get-longest-seq.sh 
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-get-longest-seq.sh
```

We then run DisEMBL on each sequence:

```bash
rm -f cmd-run-disembl.sh

while IFS="" read -r p || [ -n "$p" ]
do
  echo "python3 ~/Programs/DisEMBL-1.4/DisEMBL.py 8 8 4 1.2 1.4 1.2 Results/Bacteria/$p/DisEMBL/${p}_sequence.fasta scores > Results/Bacteria/$p/DisEMBL/${p}_scores.txt" >> cmd-run-disembl.sh 
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-run-disembl.sh
```

We then convert the output to a SGED file:

```bash
rm -f cmd-disembl2sged.sh

while IFS="" read -r p || [ -n "$p" ]
do
  echo "python3 Scripts/sged-disembl2sged.py \
    -d Results/Bacteria/$p/DisEMBL/${p}_scores.txt \
    -o Results/Bacteria/$p/DisEMBL/${p}_scores.tsv" >> cmd-disembl2sged.sh 
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-disembl2sged.sh
```

We create a sequence index:

```bash
rm -f cmd-create-sequence-index.sh

while IFS="" read -r p || [ -n "$p" ]
do
  seq=`grep '>' Results/Bacteria/$p/DisEMBL/${p}_sequence.fasta | sed 's/>//'`
  echo "Reference sequence: $seq"
  echo "python3 Scripts/sged-create-sequence-index.py \
    --alignment Families/Bacteria/$p/${p}_bppalnscore.mase \
    --reference $seq \
    --output Results/Bacteria/$p/DisEMBL/${p}_SeqIndex.txt" >> cmd-create-sequence-index.sh 
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-create-sequence-index.sh
```

### Merge output files with PDB infos:


```bash
rm -f cmd-merge-structure-infos.sh

while IFS="" read -r p || [ -n "$p" ]
do
  INFOSFILE=Results/Bacteria/$p/${p}_structureSites_infos2.tsv
  if [ ! -f $INFOSFILE ]; then
    INFOSFILE=Results/Bacteria/$p/${p}_structureSites_infos.tsv
  fi
  for method in ind1 ind2 ind3 ind4 ind5 ind6 ind7 ind8 vol pol charge
  do
    echo "python Scripts/sged-merge.py \
       --sged1=Results/Bacteria/$p/${p}_${method}_siteInfos_PDB.tsv \
       --sged2=$INFOSFILE \
       --group=PDB \
       --join=left \
       --output=Results/Bacteria/$p/${p}_${method}_siteInfos_PDB_infos.tsv >& Results/Bacteria/$p/${p}_${method}_MergeStructureInfos.log" >> cmd-merge-structure-infos.sh
  done     
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-merge-structure-infos.sh
```

Check that everything went ok:
```bash
grep Error Results/Bacteria/HOG000*/*MergeStructureInfos.log
grep error Results/Bacteria/HOG000*/*MergeStructureInfos.log
```

Now add intrinsic disorder predictions. First we need to translate according to the sequence used for predicting disordered regions:
```bash
rm -f cmd-translate-disembl-coords.sh

while IFS="" read -r p || [ -n "$p" ]
do
  for method in ind1 ind2 ind3 ind4 ind5 ind6 ind7 ind8 vol pol charge
  do
    echo "python3 Scripts/sged-translate-coords.py \
      --sged Results/Bacteria/$p/${p}_${method}_siteInfos_PDB_infos.tsv \
      --index Results/Bacteria/$p/DisEMBL/${p}_SeqIndex.txt \
      --output Results/Bacteria/$p/${p}_${method}_siteInfos_PDB_infos_tln.tsv \
      --name=DisEmblSeq >& Results/Bacteria/$p/${p}_${method}_TranslateDisEmblCoords.log" >> cmd-translate-disembl-coords.sh 
  done
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-translate-disembl-coords.sh
```
Check that everything went ok:
```bash
grep Error Results/Bacteria/HOG000*/*TranslateDisEmblCoords.log
grep error Results/Bacteria/HOG000*/*TranslateDisEmblCoords.log
```

Then merge!

```bash
rm -f cmd-merge-disorder-infos.sh

while IFS="" read -r p || [ -n "$p" ]
do
  for method in ind1 ind2 ind3 ind4 ind5 ind6 ind7 ind8 vol pol charge
  do
    echo "python Scripts/sged-merge.py \
       --sged1=Results/Bacteria/$p/${p}_${method}_siteInfos_PDB_infos_tln.tsv \
       --sged2=Results/Bacteria/$p/DisEMBL/${p}_scores.tsv \
       --group1=DisEmblSeq \
       --group2=Group \
       --join=left \
       --output=Results/Bacteria/$p/${p}_${method}_siteInfos_PDB_DisEMBL_infos.tsv >& Results/Bacteria/$p/${p}_${method}_MergeDisorderInfos.log" >> cmd-merge-disorder-infos.sh
  done     
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-merge-disorder-infos.sh
```
Check that everything went ok:
```bash
grep Error Results/Bacteria/HOG000*/*MergeDisorderInfos.log
grep error Results/Bacteria/HOG000*/*MergeDisorderInfos.log
```


### We do it again with discretized indices:

```bash
rm -f cmd-merge-structure-infos-discrete.sh

while IFS="" read -r p || [ -n "$p" ]
do
  INFOSFILE=Results/Bacteria/$p/${p}_structureSites_infos2.tsv
  if [ ! -f $INFOSFILE ]; then
    INFOSFILE=Results/Bacteria/$p/${p}_structureSites_infos.tsv
  fi
  for method in vol3 pol3
  do
    echo "python Scripts/sged-merge.py \
       --sged1=Results/Bacteria/$p/${p}_${method}_siteInfos_PDB.tsv \
       --sged2=$INFOSFILE \
       --group=PDB \
       --join=left \
       --output=Results/Bacteria/$p/${p}_${method}_siteInfos_PDB_infos.tsv >& Results/Bacteria/$p/${p}_${method}_MergeStructureInfosDiscrete.log" >> cmd-merge-structure-infos-discrete.sh
  done     
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-merge-structure-infos-discrete.sh
```
Check that everything went ok:
```bash
grep Error Results/Bacteria/HOG000*/*MergeStructureInfosDiscrete.log
grep error Results/Bacteria/HOG000*/*MergeStructureInfosDiscrete.log
```

Now add intrinsic disorder predictions. First we need to translate according to the sequence used for predicting disordered regions:
```bash
rm -f cmd-translate-disembl-coords-discrete.sh

while IFS="" read -r p || [ -n "$p" ]
do
  for method in vol3 pol3
  do
    echo "python3 Scripts/sged-translate-coords.py \
      --sged Results/Bacteria/$p/${p}_${method}_siteInfos_PDB_infos.tsv \
      --index Results/Bacteria/$p/DisEMBL/${p}_SeqIndex.txt \
      --output Results/Bacteria/$p/${p}_${method}_siteInfos_PDB_infos_tln.tsv \
      --name=DisEmblSeq >& Results/Bacteria/$p/${p}_${method}_TranslateDisEmblCoordsDiscrete.log" >> cmd-translate-disembl-coords-discrete.sh 
  done
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-translate-disembl-coords-discrete.sh
```
Check that everything went ok:
```bash
grep Error Results/Bacteria/HOG000*/*TranslateDisEmblCoordsDiscrete.log
grep error Results/Bacteria/HOG000*/*TranslateDisEmblCoordsDiscrete.log
```

Then merge!

```bash
rm -f cmd-merge-disorder-infos-discrete.sh

while IFS="" read -r p || [ -n "$p" ]
do
  for method in vol3 pol3
  do
    echo "python Scripts/sged-merge.py \
       --sged1=Results/Bacteria/$p/${p}_${method}_siteInfos_PDB_infos_tln.tsv \
       --sged2=Results/Bacteria/$p/DisEMBL/${p}_scores.tsv \
       --group1=DisEmblSeq \
       --group2=Group \
       --join=left \
       --output=Results/Bacteria/$p/${p}_${method}_siteInfos_PDB_DisEMBL_infos.tsv >& Results/Bacteria/$p/${p}_${method}_MergeDisorderInfosDiscrete.log" >> cmd-merge-disorder-infos-discrete.sh
  done     
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-merge-disorder-infos-discrete.sh
```
Check that everything went ok:
```bash
grep Error Results/Bacteria/HOG000*/*MergeDisorderInfosDiscrete.log
grep error Results/Bacteria/HOG000*/*MergeDisorderInfosDiscrete.log
```




## Create a list of coevolving sites

First, retrieve only significant groups:
```bash
rm -f cmd-significant.sh

while IFS="" read -r p || [ -n "$p" ]
do
  for method in ind1 ind2 ind3 ind4 ind5 ind6 ind7 ind8 vol pol charge
  do
    if [ -f Results/Bacteria/$p/${p}_${method}_stats_pvalues.csv ]
    then
      echo "csvgrep -t -c FDR -m yes Results/Bacteria/$p/${p}_${method}_stats_pvalues.csv | csvformat -T > Results/Bacteria/$p/${p}_${method}_significant.tsv" >> cmd-significant.sh
    else
      #Creates an empty file with headers only
      echo -e "Group\tSize\tIsConstant\tDmax\tStat\tNmin\tp.value\tnobs\tcode\tMethod\tFDR" > Results/Bacteria/$p/${p}_${method}_significant.tsv
    fi
  done
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-significant.sh
```

Second, get the list of sites within these groups:
```bash
rm -f cmd-ungroup.sh

while IFS="" read -r p || [ -n "$p" ]
do
  for method in ind1 ind2 ind3 ind4 ind5 ind6 ind7 ind8 vol pol charge
  do
    echo "python Scripts/sged-ungroup.py \
         --sged=Results/Bacteria/$p/${p}_${method}_significant.tsv \
         --data=Size,Stat,Nmin,p.value,nobs,FDR \
         --output=Results/Bacteria/$p/${p}_${method}_stats_pvalues_sites.tsv >& Results/Bacteria/$p/${p}_${method}_Ungroup.log" >> cmd-ungroup.sh
  done
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-ungroup.sh
```

Check that everything went ok:
```bash
grep Error Results/Bacteria/HOG000*/*Ungroup.log
grep error Results/Bacteria/HOG000*/*Ungroup.log
```

### Same for discretized indices:

First, retrieve only significant groups:
```bash
rm -f cmd-significant-discrete.sh

while IFS="" read -r p || [ -n "$p" ]
do
  for method in vol3 pol3
  do
    if [ -f Results/Bacteria/$p/${p}_${method}_stats_pvalues.csv ]
    then
      echo "csvgrep -t -c FDR -m yes Results/Bacteria/$p/${p}_${method}_stats_pvalues.csv | csvformat -T > Results/Bacteria/$p/${p}_${method}_significant.tsv" >> cmd-significant-discrete.sh
    else
      #Creates an empty file with headers only
      echo -e "Group\tSize\tIsConstant\tDmax\tStat\tNmin\tp.value\tnobs\tcode\tMethod\tFDR" > Results/Bacteria/$p/${p}_${method}_significant.tsv
    fi
  done
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-significant-discrete.sh
```

Second, get the list of sites within these groups:
```bash
rm -f cmd-ungroup-discrete.sh

while IFS="" read -r p || [ -n "$p" ]
do
  for method in vol3 pol3
  do
    echo "python Scripts/sged-ungroup.py \
         --sged=Results/Bacteria/$p/${p}_${method}_significant.tsv \
         --data=Size,Stat,Nmin,p.value,nobs,FDR \
         --output=Results/Bacteria/$p/${p}_${method}_stats_pvalues_sites.tsv >& Results/Bacteria/$p/${p}_${method}_UngroupDiscrete.log" >> cmd-ungroup-discrete.sh
  done
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-ungroup-discrete.sh
```

Check that everything went ok:
```bash
grep Error Results/Bacteria/HOG000*/*UngroupDiscrete.log
grep error Results/Bacteria/HOG000*/*UngroupDiscrete.log
```

## Combine structural information with coevolution

```bash
rm -f cmd-merge.sh

while IFS="" read -r p || [ -n "$p" ]
do
  for method in ind1 ind2 ind3 ind4 ind5 ind6 ind7 ind8 vol pol charge
  do
    echo "python Scripts/sged-merge.py \
       --sged1=Results/Bacteria/$p/${p}_${method}_siteInfos_PDB_DisEMBL_infos.tsv \
       --sged2=Results/Bacteria/$p/${p}_${method}_stats_pvalues_sites.tsv \
       --group1=Group_x \
       --group2=Group \
       --join=outer \
       --output=Results/Bacteria/$p/${p}_${method}_merged.tsv > Results/Bacteria/$p/${p}_${method}_Merge.log 2>&1" >> cmd-merge.sh
  done
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-merge.sh
```

Check that everything went ok:
```bash
grep Error Results/Bacteria/HOG000*/*Merge.log
grep error Results/Bacteria/HOG000*/*Merge.log
```

### And for discretized indices

```bash
rm -f cmd-merge-discrete.sh

while IFS="" read -r p || [ -n "$p" ]
do
  for method in vol3 pol3
  do
    echo "python Scripts/sged-merge.py \
       --sged1=Results/Bacteria/$p/${p}_${method}_siteInfos_PDB_DisEMBL_infos.tsv \
       --sged2=Results/Bacteria/$p/${p}_${method}_stats_pvalues_sites.tsv \
       --group1=Group_x \
       --group2=Group \
       --join=outer \
       --output=Results/Bacteria/$p/${p}_${method}_merged.tsv > Results/Bacteria/$p/${p}_${method}_MergeDiscrete.log 2>&1" >> cmd-merge-discrete.sh
  done
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-merge-discrete.sh
```

Check that everything went ok:
```bash
grep Error Results/Bacteria/HOG000*/*MergeDiscrete.log
grep error Results/Bacteria/HOG000*/*MergeDiscrete.log
```



## Combine results from all families

```bash
Rscript Scripts/combineAllMethodsPerFamily.R
gzip Results/Bacteria/AllFamilies_sites_casted.csv
```

```bash
Rscript Scripts/combineAllMethodsAllFamilies.R
gzip Results/Bacteria/AllFamilies_sites_molted.csv
```

Statistical analyses in SiteAnalysis.R!







Group analyses
==============

## We add the structural information for each group:

```bash
rm -f cmd-translate-group-coords.sh

while IFS="" read -r p || [ -n "$p" ]
do
  for method in ind1 ind2 ind3 ind4 ind5 ind6 ind7 ind8 vol pol charge
  do
    echo "python Scripts/sged-translate-coords.py \
           --sged=Results/Bacteria/$p/${p}_${method}_significant.tsv \
           --index=Results/Bacteria/$p/${p}_StructureIndex.txt \
           --output=Results/Bacteria/$p/${p}_${method}_significant_PDB.tsv \
           --name=PDB >& Results/Bacteria/$p/${p}_${method}_TranslateGroupCoords.log" >> cmd-translate-group-coords.sh
  done
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-translate-group-coords.sh

```
Check that everything went ok:
```bash
grep Error Results/Bacteria/HOG000*/*TranslateGroupCoords.log
grep error Results/Bacteria/HOG000*/*TranslateGroupCoords.log
```

### Do the same for discretized indices:

```bash
rm -f cmd-translate-group-coords-discrete.sh

while IFS="" read -r p || [ -n "$p" ]
do
  for method in vol3 pol3
  do
    echo "python Scripts/sged-translate-coords.py \
           --sged=Results/Bacteria/$p/${p}_${method}_significant.tsv \
           --index=Results/Bacteria/$p/${p}_StructureIndex.txt \
           --output=Results/Bacteria/$p/${p}_${method}_significant_PDB.tsv \
           --name=PDB >& Results/Bacteria/$p/${p}_${method}_TranslateGroupCoordsDiscrete.log" >> cmd-translate-group-coords-discrete.sh
  done
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-translate-group-coords-discrete.sh

```
Check that everything went ok:
```bash
grep Error Results/Bacteria/HOG000*/*TranslateGroupCoordsDiscrete.log
grep error Results/Bacteria/HOG000*/*TranslateGroupCoordsDiscrete.log
```

## We compute compute the structural information for each analysed group:

```bash
rm -f cmd-group-structure-infos.sh

while IFS="" read -r p || [ -n "$p" ]
do
  rm -f Families/Bacteria/$p/*_model0*
  # Get the best PDB structure used for the index:
  PDBFILE=`cat Results/Bacteria/$p/${p}_StructureIndex.txt | grep "# SGED input PDB = " | sed "s/# SGED input PDB = \(.*\)/\1/"`
  PDBCHAIN=`cat Results/Bacteria/$p/${p}_StructureIndex.txt | grep "# SGED input PDB chain = " | sed "s/# SGED input PDB chain = \(.*\)/\1/"`

  for method in ind1 ind2 ind3 ind4 ind5 ind6 ind7 ind8 vol pol charge
  do
    echo "python3 Scripts/sged-structure-infos.py \
         --sged=Results/Bacteria/$p/${p}_${method}_significant_PDB.tsv \
         --group=PDB \
         --pdb=$PDBFILE \
         --chain=$PDBCHAIN \
         --measures=AlphaDist,ContactSubgraphs,DSSPsum \
         --output=Results/Bacteria/$p/${p}_${method}_significant_PDB_infos.tsv >& Results/Bacteria/$p/${p}_${method}_GroupStructureInfos.log" >> cmd-group-structure-infos.sh
  done
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-group-structure-infos.sh

```

Check that everything went ok:
```bash
grep Error Results/Bacteria/HOG000*/*GroupStructureInfos.log
grep error Results/Bacteria/HOG000*/*GroupStructureInfos.log
grep ERROR Results/Bacteria/HOG000*/*GroupStructureInfos.log
```
=> 3 where DSSP could not be run: HOG000035157, HOG000219148, HOG000231585 (all ribosomal proteins, only Ca available)

### Do the same for the discrete indices:

```bash
rm -f cmd-group-structure-infos-discrete.sh

while IFS="" read -r p || [ -n "$p" ]
do
  rm -f Families/Bacteria/$p/*_model0*
  # Get the best PDB structure used for the index:
  PDBFILE=`cat Results/Bacteria/$p/${p}_StructureIndex.txt | grep "# SGED input PDB = " | sed "s/# SGED input PDB = \(.*\)/\1/"`
  PDBCHAIN=`cat Results/Bacteria/$p/${p}_StructureIndex.txt | grep "# SGED input PDB chain = " | sed "s/# SGED input PDB chain = \(.*\)/\1/"`

  for method in vol3 pol3
  do
    echo "python3 Scripts/sged-structure-infos.py \
         --sged=Results/Bacteria/$p/${p}_${method}_significant_PDB.tsv \
         --group=PDB \
         --pdb=$PDBFILE \
         --chain=$PDBCHAIN \
         --measures=AlphaDist,ContactSubgraphs,DSSPsum \
         --output=Results/Bacteria/$p/${p}_${method}_significant_PDB_infos.tsv >& Results/Bacteria/$p/${p}_${method}_GroupStructureInfosDiscrete.log" >> cmd-group-structure-infos-discrete.sh
  done
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-group-structure-infos-discrete.sh

```

Check that everything went ok:
```bash
grep Error Results/Bacteria/HOG000*/*GroupStructureInfosDiscrete.log
grep error Results/Bacteria/HOG000*/*GroupStructureInfosDiscrete.log
grep ERROR Results/Bacteria/HOG000*/*GroupStructureInfosDiscrete.log
```

## Compute secondary structure labels

```bash
rm -f cmd-group-structure-infos2.sh

while IFS="" read -r p || [ -n "$p" ]
do
  rm -f Families/Bacteria/$p/*_model0*
  # Get the best PDB structure used for the index:
  PDBID=`cat Results/Bacteria/$p/${p}_StructureIndex.txt | grep "# SGED input PDB = " | sed "s/# SGED input PDB = \(.*\).pdb/\1/"`
  PDBCHAIN=`cat Results/Bacteria/$p/${p}_StructureIndex.txt | grep "# SGED input PDB chain = " | sed "s/# SGED input PDB chain = \(.*\)/\1/"`


  for method in ind1 ind2 ind3 ind4 ind5 ind6 ind7 ind8 vol pol charge
  do
    echo "python3 Scripts/sged-structure-infos.py \
         --sged=Results/Bacteria/$p/${p}_${method}_significant_PDB_infos.tsv \
         --group=PDB \
         --pdb=${PDBID}.cif \
         --pdb-format=mmCIF \
         --chain=$PDBCHAIN \
         --measures=SecondaryStructureLabel \
         --output=Results/Bacteria/$p/${p}_${method}_significant_PDB_infos2.tsv >& Results/Bacteria/$p/${p}_${method}_GroupStructureInfos2.log" >> cmd-group-structure-infos2.sh
  done
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-group-structure-infos2.sh

```

Check that everything went ok:
```bash
grep Error Results/Bacteria/HOG000*/*GroupStructureInfos2.log
grep error Results/Bacteria/HOG000*/*GroupStructureInfos2.log
grep ERROR Results/Bacteria/HOG000*/*GroupStructureInfos2.log
```
=> 16 families have a strange mmCIF file with annotations spanning several subunits.

### Do the same for the discrete indices:

```bash
rm -f cmd-group-structure-infos-discrete.sh

while IFS="" read -r p || [ -n "$p" ]
do
  rm -f Families/Bacteria/$p/*_model0*
  # Get the best PDB structure used for the index:
  PDBID=`cat Results/Bacteria/$p/${p}_StructureIndex.txt | grep "# SGED input PDB = " | sed "s/# SGED input PDB = Families\/Bacteria\/HOG.*\/\(.*\).pdb/\1/"`
  PDBCHAIN=`cat Results/Bacteria/$p/${p}_StructureIndex.txt | grep "# SGED input PDB chain = " | sed "s/# SGED input PDB chain = \(.*\)/\1/"`

  for method in vol3 pol3
  do
    echo "cd Results/Bacteria/$p/; python3 Scripts/sged-structure-infos.py \
         --sged=Results/Bacteria/$p/${p}_${method}_significant_PDB.tsv \
         --group=PDB \
         --pdb=$PDBFILE \
         --chain=$PDBCHAIN \
         --measures=AlphaDist,ContactSubgraphs,DSSPsum \
         --output=Results/Bacteria/$p/${p}_${method}_significant_PDB_infos.tsv >& Results/Bacteria/$p/${p}_${method}_GroupStructureInfosDiscrete.log" >> cmd-group-structure-infos-discrete.sh
  done
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-group-structure-infos-discrete.sh

```

Check that everything went ok:
```bash
grep Error Results/Bacteria/HOG000*/*GroupStructureInfosDiscrete.log
grep error Results/Bacteria/HOG000*/*GroupStructureInfosDiscrete.log
grep ERROR Results/Bacteria/HOG000*/*GroupStructureInfosDiscrete.log
```

## Compute secondary structure labels

```bash
rm -f cmd-group-structure-infos2-discrete.sh

while IFS="" read -r p || [ -n "$p" ]
do
  rm -f Families/Bacteria/$p/*_model0*
  # Get the best PDB structure used for the index:
  PDBID=`cat Results/Bacteria/$p/${p}_StructureIndex.txt | grep "# SGED input PDB = " | sed "s/# SGED input PDB = \(.*\).pdb/\1/"`
  PDBCHAIN=`cat Results/Bacteria/$p/${p}_StructureIndex.txt | grep "# SGED input PDB chain = " | sed "s/# SGED input PDB chain = \(.*\)/\1/"`


  for method in vol3 pol3
  do
    echo "python3 Scripts/sged-structure-infos.py \
         --sged=Results/Bacteria/$p/${p}_${method}_significant_PDB_infos.tsv \
         --group=PDB \
         --pdb=${PDBID}.cif \
         --pdb-format=mmCIF \
         --chain=$PDBCHAIN \
         --measures=SecondaryStructureLabel \
         --output=Results/Bacteria/$p/${p}_${method}_significant_PDB_infos2.tsv >& Results/Bacteria/$p/${p}_${method}_GroupStructureInfos2Discrete.log" >> cmd-group-structure-infos2-discrete.sh
  done
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-group-structure-infos2-discrete.sh

```

Check that everything went ok:
```bash
grep Error Results/Bacteria/HOG000*/*GroupStructureInfos2Discrete.log
grep error Results/Bacteria/HOG000*/*GroupStructureInfos2Discrete.log
grep ERROR Results/Bacteria/HOG000*/*GroupStructureInfos2Discrete.log
```
=> 16 families have a strange mmCIF file with annotations spanning several subunits.


## Merge all results into a single file:

```bash
Rscript Scripts/combineGroupsAllMethodsAllFamilies.R
gzip Results/Bacteria/AllFamilies_groups_molted.csv
```


Prediction of residues in contact
=================================

For each family, we list all pairs of residues and their respective Ca distance.

First we get all pairs of sites in the PDB:

```bash
rm -f cmd-all-pairwise-structure-sites.sh

while IFS="" read -r p || [ -n "$p" ]
do
  rm -f Families/Bacteria/$p/*_model0*
  echo "python3 Scripts/sged-get-all-pairs.py \
        --sged=Results/Bacteria/$p/${p}_structureSites.tsv \
        --group=PDB \
        --output=Results/Bacteria/$p/${p}_StructureSitePairs.tsv >& Results/Bacteria/$p/${p}_StructureSitePairs.log" >> cmd-all-pairwise-structure-sites.sh
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-all-pairwise-structure-sites.sh

```

We check that all went well:
```bash
grep Error Results/Bacteria/HOG000*/*StructureSitePairs.log
grep error Results/Bacteria/HOG000*/*StructureSitePairs.log
grep ERROR Results/Bacteria/HOG000*/*StructureSitePairs.log
```

Then we compute all Ca-Ca distances, as well as other structural variable:

```bash
rm -f cmd-all-pairwise-structure-infos.sh

while IFS="" read -r p || [ -n "$p" ]
do
  rm -f Families/Bacteria/$p/*_model0*
  # Get the best PDB structure used for the index:
  PDBFILE=`cat Results/Bacteria/$p/${p}_StructureIndex.txt | grep "# SGED input PDB = " | sed "s/# SGED input PDB = \(.*\)/\1/"`
  PDBCHAIN=`cat Results/Bacteria/$p/${p}_StructureIndex.txt | grep "# SGED input PDB chain = " | sed "s/# SGED input PDB chain = \(.*\)/\1/"`

  echo "python3 Scripts/sged-structure-infos.py \
        --sged=Results/Bacteria/$p/${p}_StructureSitePairs.tsv \
        --group=PDB \
        --pdb=$PDBFILE \
        --chain=$PDBCHAIN \
        --measures=AlphaDist,DSSPsum \
        --output=Results/Bacteria/$p/${p}_structureSitesPairs_infos.tsv >& Results/Bacteria/$p/${p}_StructureSitePairsInfos.log" >> cmd-all-pairwise-structure-infos.sh
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-all-pairwise-structure-infos.sh

```

We check that all went well:
```bash
grep Error Results/Bacteria/HOG000*/*StructureSitePairsInfos.log
grep error Results/Bacteria/HOG000*/*StructureSitePairsInfos.log
grep ERROR Results/Bacteria/HOG000*/*StructureSitePairsInfos.log
```

=> 3 where DSSP could not be run:
HOG000035157 
HOG000219148
HOG000231585
(all ribosomal proteins, only Ca available)

Now we test, for each method, whether each pair is included in a coevolving group.
We also need to add the rate for each site. We use the PDB position as index.

```bash
rm -f cmd-all-pairwise-coevolution.sh

while IFS="" read -r p || [ -n "$p" ]
do
  for method in ind1 ind2 ind3 ind4 ind5 ind6 ind7 ind8 vol pol charge
  do
    echo "python Scripts/sged-group-test-inclusion.py \
         --sged1=Results/Bacteria/$p/${p}_structureSitesPairs_infos.tsv \
         --group1=PDB \
         --sged2=Results/Bacteria/$p/${p}_${method}_significant_PDB.tsv \
         --group2=PDB \
         --result=IsCoevolving \
         --output=Results/Bacteria/$p/${p}_structureSitesPairs_infos_${method}.tsv >& Results/Bacteria/$p/${p}_${method}_StructureSitePairsCoevolution.log" >> cmd-all-pairwise-coevolution.sh
  done
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-all-pairwise-coevolution.sh

```

We check that all went well:
```bash
grep Error Results/Bacteria/HOG000*/*StructureSitePairsCoevolution.log
grep error Results/Bacteria/HOG000*/*StructureSitePairsCoevolution.log
grep ERROR Results/Bacteria/HOG000*/*StructureSitePairsCoevolution.log
```


## Combine results from all families

```bash
Rscript Scripts/combineStructureSitePairsAllMethodsPerFamily.R
```

This leads to a 12G file with >92e6 lines! We need to sample positions randomly.

We keep all coevolving pairs, and add as many non-coevolving ones, taken randomly.
```bash
Rscript Scripts/combineStructureSitePairsAllMethodsPerFamilySampling.R
```

This approach could be biased, ignoring the fact that coevolving pairs are only a minority... 
We sampled randomly per family, trying to keep equal numbers per family.
```bash
Rscript Scripts/combineStructureSitePairsAllMethodsPerFamilySampling2.R
```


# Get family statistics:

```r
# Read trees:
require(ape)
lst <- list.files("Families/Bacteria", recursive = TRUE, pattern = "*sample_2.dnd", full.names = TRUE)
trees <- list()
for (file in lst) {
  f <- strsplit(file, "/")[[1]][3]
  trees[[f]] <- read.tree(file)
}

# Compute statistics:
require(plyr)
dat <- ldply(.data = trees, .id = "Family", .fun = function(tree) {
  total <- sum(tree$edge.length)
  diameter <- max(cophenetic(tree))
  return(data.frame(TreeTotalLength = total, TreeDiameter = diameter))
})

# Add sequence infos:
require(seqinr)
for (i in 1:nrow(dat)) {
  fam <- dat[i, "Family"]
  file <- paste("Families/Bacteria/", fam, "/", fam, "_bppalnscore.mase", sep = "")
  aln <- read.alignment(file, format = "mase")
  dat[i, "SequenceMedianLength"] <- median(sapply(aln$seq, function(x) sum(s2c(x) != "-")))
}

write.table(dat, file="FamilyStatistics.tsv", sep = "\t", row.names = FALSE)
```

Randomization
=============

## Without condition

```bash
NSIM=100

rm -f cmd-randomize.sh

while IFS="" read -r p || [ -n "$p" ]
do
  mkdir Results/Bacteria/$p/Simulations
  for method in ind1 ind2 ind3 ind4 ind5 ind6 ind7 ind8 vol pol charge
  do
    if [ -f Results/Bacteria/$p/${p}_${method}_Randomize.log ]; then
      rm Results/Bacteria/$p/${p}_${method}_Randomize.log
    fi
    echo "python3 Scripts/sged-randomize-groups.py \
         --sged-groups=Results/Bacteria/$p/${p}_${method}_significant.tsv \
         --sged-sites=Results/Bacteria/$p/${p}_${method}_siteInfos_PDB_infos.tsv \
         --measure=N \
         --number-replicates=$NSIM \
         --similarity-threshold=100000 \
         --output=Results/Bacteria/$p/Simulations/${p}_${method}_random.tsv > Results/Bacteria/$p/${p}_${method}_Randomize.log 2>&1" >> cmd-randomize.sh
  done
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-randomize.sh
```

Merge all results into a single file, one per method:

```bash
Rscript Scripts/combineRandomizationAllFamilies.R "Simulations" "random"
gzip Results/Bacteria/AllFamilies_groups_*_random.csv
```

Another set with RSA instead:

```bash
NSIM=100

rm -f cmd-randomize-rsa.sh

while IFS="" read -r p || [ -n "$p" ]
do
  mkdir Results/Bacteria/$p/Simulations
  for method in ind1 ind2 ind3 ind4 ind5 ind6 ind7 ind8 vol pol charge
  do
    if [ -f Results/Bacteria/$p/${p}_${method}_RandomizeRsa.log ]; then
      rm Results/Bacteria/$p/${p}_${method}_RandomizeRsa.log
    fi
    echo "python3 Scripts/sged-randomize-groups.py \
         --sged-groups=Results/Bacteria/$p/${p}_${method}_significant.tsv \
         --sged-sites=Results/Bacteria/$p/${p}_${method}_siteInfos_PDB_infos.tsv \
         --measure=Rsa \
         --number-replicates=$NSIM \
         --similarity-threshold=100000 \
         --output=Results/Bacteria/$p/Simulations/${p}_${method}_random-rsa.tsv > Results/Bacteria/$p/${p}_${method}_RandomizeRsa.log 2>&1" >> cmd-randomize-rsa.sh
  done
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-randomize-rsa.sh
```

Merge all results into a single file, one per method:

```bash
Rscript Scripts/combineRandomizationAllFamilies.R "Simulations" "random-rsa"
gzip Results/Bacteria/AllFamilies_groups_*_random-rsa.csv
```



## Conditionned on rate

With a 10% similarity threshold for the rate:
```bash
NSIM=100

rm -f cmd-randomize-rate10.sh

while IFS="" read -r p || [ -n "$p" ]
do
  mkdir Results/Bacteria/$p/SimulationsRate
  for method in ind1 ind2 ind3 ind4 ind5 ind6 ind7 ind8 vol pol charge
  do
    if [ -f Results/Bacteria/$p/${p}_${method}_RandomizeRate10.log ]; then
      rm Results/Bacteria/$p/${p}_${method}_RandomizeRate10.log
    fi
    echo "python3 Scripts/sged-randomize-groups.py \
         --sged-groups=Results/Bacteria/$p/${p}_${method}_significant.tsv \
         --sged-sites=Results/Bacteria/$p/${p}_${method}_siteInfos_PDB_infos.tsv \
         --measure=N \
         --number-replicates=$NSIM \
         --similarity-threshold=0.1 \
         --output=Results/Bacteria/$p/SimulationsRate/${p}_${method}_random-rates-10.tsv > Results/Bacteria/$p/${p}_${method}_RandomizeRate10.log 2>&1" >> cmd-randomize-rate10.sh
  done
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-randomize-rate10.sh
```

Merge all results into a single file, one per method:

```bash
Rscript Scripts/combineRandomizationAllFamilies.R "SimulationsRate" "random-rates-10"
gzip Results/Bacteria/AllFamilies_groups_*_random-rates-10.csv
```

We compare with a threshold of 20%:
```bash
NSIM=100

rm -f cmd-randomize-rate20.sh

while IFS="" read -r p || [ -n "$p" ]
do
  mkdir Results/Bacteria/$p/SimulationsRate
  for method in ind1 ind2 ind3 ind4 ind5 ind6 ind7 ind8 vol pol charge
  do
    if [ -f Results/Bacteria/$p/${p}_${method}_RandomizeRate20.log ]; then
      rm Results/Bacteria/$p/${p}_${method}_RandomizeRate20.log
    fi
    echo "python3 Scripts/sged-randomize-groups.py \
         --sged-groups=Results/Bacteria/$p/${p}_${method}_significant.tsv \
         --sged-sites=Results/Bacteria/$p/${p}_${method}_siteInfos_PDB_infos.tsv \
         --measure=N \
         --number-replicates=$NSIM \
         --similarity-threshold=0.2 \
         --output=Results/Bacteria/$p/SimulationsRate/${p}_${method}_random-rates-20.tsv > Results/Bacteria/$p/${p}_${method}_RandomizeRate20.log 2>&1" >> cmd-randomize-rate20.sh
  done
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-randomize-rate20.sh
```

Merge all results into a single file, one per method:

```bash
Rscript Scripts/combineRandomizationAllFamilies.R "SimulationsRate" "random-rates-20"
gzip Results/Bacteria/AllFamilies_groups_*_random-rates-20.csv
```

### Add structural information

We use the 10% threshold. We run 1000 permutations:

```bash
NSIM=1000

rm -f cmd-randomize-rate10-1k.sh

while IFS="" read -r p || [ -n "$p" ]
do
  mkdir Results/Bacteria/$p/SimulationsRate
  for method in ind1 ind2 ind3 ind4 ind5 ind6 ind7 ind8 vol pol charge
  do
    if [ -f Results/Bacteria/$p/${p}_${method}_RandomizeRate10-1k.log ]; then
      rm Results/Bacteria/$p/${p}_${method}_RandomizeRate10-1k.log
    fi
    echo "python3 Scripts/sged-randomize-groups.py \
         --sged-groups=Results/Bacteria/$p/${p}_${method}_significant.tsv \
         --sged-sites=Results/Bacteria/$p/${p}_${method}_siteInfos_PDB_infos.tsv \
         --measure=N \
         --number-replicates=$NSIM \
         --similarity-threshold=0.1 \
         --output=Results/Bacteria/$p/SimulationsRate/${p}_${method}_random-rates-10-1k.tsv > Results/Bacteria/$p/${p}_${method}_RandomizeRate10-1k.log 2>&1" >> cmd-randomize-rate10-1k.sh
  done
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-randomize-rate10-1k.sh
```

Then add the structural coordinates for each random group:
```bash
rm -f cmd-randomize-rate10-translate-group-coords.sh

while IFS="" read -r p || [ -n "$p" ]
do
  for method in ind1 ind2 ind3 ind4 ind5 ind6 ind7 ind8 vol pol charge
  do
    echo "python Scripts/sged-translate-coords.py \
           --sged=Results/Bacteria/$p/SimulationsRate/${p}_${method}_random-rates-10-1k.tsv \
           --index=Results/Bacteria/$p/${p}_StructureIndex.txt \
           --output=Results/Bacteria/$p/SimulationsRate/${p}_${method}_random-rates-10_PDB.tsv \
           --name=PDB >& Results/Bacteria/$p/${p}_${method}_RandomizeRate10_TranslateGroupCoords.log" >> cmd-randomize-rate10-translate-group-coords.sh
  done
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-randomize-rate10-translate-group-coords.sh

```
Check that everything went ok:
```bash
grep Error Results/Bacteria/HOG000*/*RandomizeRate10_TranslateGroupCoords.log
grep error Results/Bacteria/HOG000*/*RandomizeRate10_TranslateGroupCoords.log
```

And then the 3D statistics:
```bash
rm -f cmd-randomize-rate10-structure-infos.sh

while IFS="" read -r p || [ -n "$p" ]
do
  rm -f Families/Bacteria/$p/*_model0*
  # Get the best PDB structure used for the index:
  PDBFILE=`cat Results/Bacteria/$p/${p}_StructureIndex.txt | grep "# SGED input PDB = " | sed "s/# SGED input PDB = \(.*\)/\1/"`
  PDBCHAIN=`cat Results/Bacteria/$p/${p}_StructureIndex.txt | grep "# SGED input PDB chain = " | sed "s/# SGED input PDB chain = \(.*\)/\1/"`

  for method in ind1 ind2 ind3 ind4 ind5 ind6 ind7 ind8 vol pol charge
  do
    echo "python3 Scripts/sged-structure-infos.py \
         --sged=Results/Bacteria/$p/SimulationsRate/${p}_${method}_random-rates-10_PDB.tsv \
         --group=PDB \
         --pdb=$PDBFILE \
         --chain=$PDBCHAIN \
         --measures=AlphaDist,ContactSubgraphs,DSSPsum \
         --output=Results/Bacteria/$p/SimulationsRate/${p}_${method}_random-rates-10_PDB_infos.tsv >& Results/Bacteria/$p/${p}_${method}_RandomizeRate10_GroupStructureInfos.log" >> cmd-randomize-rate10-structure-infos.sh
  done
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-randomize-rate10-structure-infos.sh

```

Check that everything went ok:
```bash
grep Error Results/Bacteria/HOG000*/*RandomizeRate10_GroupStructureInfos.log
grep error Results/Bacteria/HOG000*/*RandomizeRate10_GroupStructureInfos.log
grep ERROR Results/Bacteria/HOG000*/*RandomizeRate10_GroupStructureInfos.log
```
=> 3 where DSSP could not be run: HOG000035157, HOG000219148, HOG000231585 (all ribosomal proteins, only Ca available)

Add secondary structure labelling:
```bash
rm -f cmd-randomize-rate10-structure-infos2.sh

while IFS="" read -r p || [ -n "$p" ]
do
  rm -f Families/Bacteria/$p/*_model0*
  # Get the best PDB structure used for the index:
  PDBID=`cat Results/Bacteria/$p/${p}_StructureIndex.txt | grep "# SGED input PDB = " | sed "s/# SGED input PDB = \(.*\).pdb/\1/"`
  PDBCHAIN=`cat Results/Bacteria/$p/${p}_StructureIndex.txt | grep "# SGED input PDB chain = " | sed "s/# SGED input PDB chain = \(.*\)/\1/"`

  for method in ind1 ind2 ind3 ind4 ind5 ind6 ind7 ind8 vol pol charge
  do
    echo "python3 Scripts/sged-structure-infos.py \
         --sged=Results/Bacteria/$p/SimulationsRate/${p}_${method}_random-rates-10_PDB_infos.tsv \
         --group=PDB \
         --pdb=${PDBID}.cif \
         --pdb-format=mmCIF \
         --chain=$PDBCHAIN \
         --measures=SecondaryStructureLabel \
         --output=Results/Bacteria/$p/SimulationsRate/${p}_${method}_random-rates-10_PDB_infos2.tsv >& Results/Bacteria/$p/${p}_${method}_RandomizeRate10_GroupStructureInfos2.log" >> cmd-randomize-rate10-structure-infos2.sh
  done
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-randomize-rate10-structure-infos2.sh

```

Check that everything went ok:
```bash
grep Error Results/Bacteria/HOG000*/*RandomizeRate10_GroupStructureInfos2.log
grep error Results/Bacteria/HOG000*/*RandomizeRate10_GroupStructureInfos2.log
grep ERROR Results/Bacteria/HOG000*/*RandomizeRate10_GroupStructureInfos2.log
```


Merge all results into a single file, one per method:
```bash
Rscript Scripts/combineRandomizationAllFamilies.R "SimulationsRate" "random-rates-10_PDB_infos" "random-rates-10_PDB_infos2"
gzip Results/Bacteria/AllFamilies_groups_*_random-rates-10_PDB_infos.csv
```
Note: in case secondary structure labelling is not present, then use the basic secondary structure info and fill with NA.
```
HOG000003917
HOG000005156
HOG000014316
HOG000141611
HOG000157010
HOG000175517
HOG000226033
HOG000228137
HOG000235659
HOG000248742
HOG000252083
HOG000265022
HOG000268006
HOG000283907
```

## Conditionned on RSA

With a 10% similarity threshold for the RSA:
```bash
NSIM=100

rm -f cmd-randomize-rsa10.sh

while IFS="" read -r p || [ -n "$p" ]
do
  mkdir Results/Bacteria/$p/SimulationsRsa
  for method in ind1 ind2 ind3 ind4 ind5 ind6 ind7 ind8 vol pol charge
  do
    if [ -f Results/Bacteria/$p/${p}_${method}_RandomizeRsa10.log ]; then
      rm Results/Bacteria/$p/${p}_${method}_RandomizeRsa10.log
    fi
    echo "python3 Scripts/sged-randomize-groups.py \
         --sged-groups=Results/Bacteria/$p/${p}_${method}_significant.tsv \
         --sged-sites=Results/Bacteria/$p/${p}_${method}_siteInfos_PDB_infos.tsv \
         --measure=Rsa \
         --number-replicates=$NSIM \
         --similarity-threshold=0.1 \
         --output=Results/Bacteria/$p/SimulationsRsa/${p}_${method}_random-rsa-10.tsv > Results/Bacteria/$p/${p}_${method}_RandomizeRsa10.log 2>&1" >> cmd-randomize-rsa10.sh
  done
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-randomize-rsa10.sh
```

Merge all results into a single file, one per method:

```bash
Rscript Scripts/combineRandomizationAllFamilies.R "SimulationsRsa" "random-rsa-10"
gzip Results/Bacteria/AllFamilies_groups_*_random-rsa-10.csv
```

We compare with a threshold of 20%:

```bash
NSIM=100

rm -f cmd-randomize-rsa20.sh

while IFS="" read -r p || [ -n "$p" ]
do
  for method in ind1 ind2 ind3 ind4 ind5 ind6 ind7 ind8 vol pol charge
  do
    mkdir Results/Bacteria/$p/SimulationsRsa
    if [ -f Results/Bacteria/$p/${p}_${method}_RandomizeRsa20.log ]; then
      rm Results/Bacteria/$p/${p}_${method}_RandomizeRsa20.log
    fi
    echo "python3 Scripts/sged-randomize-groups.py \
         --sged-groups=Results/Bacteria/$p/${p}_${method}_significant.tsv \
         --sged-sites=Results/Bacteria/$p/${p}_${method}_siteInfos_PDB_infos.tsv \
         --measure=Rsa \
         --number-replicates=$NSIM \
         --similarity-threshold=0.2 \
         --output=Results/Bacteria/$p/SimulationsRsa/${p}_${method}_random-rsa-20.tsv > Results/Bacteria/$p/${p}_${method}_RandomizeRsa20.log 2>&1" >> cmd-randomize-rsa20.sh
  done
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-randomize-rsa20.sh
```

Merge all results into a single file, one per method:

```bash
Rscript Scripts/combineRandomizationAllFamilies.R "SimulationsRsa" "random-rsa-20"
gzip Results/Bacteria/AllFamilies_groups_*_random-rsa-20.csv
```

### Add structural information

We use the 10% threshold. We run 1000 permutations:

```bash
NSIM=1000

rm -f cmd-randomize-rsa10-1k.sh

while IFS="" read -r p || [ -n "$p" ]
do
  mkdir Results/Bacteria/$p/SimulationsRsa
  for method in ind1 ind2 ind3 ind4 ind5 ind6 ind7 ind8 vol pol charge
  do
    if [ -f Results/Bacteria/$p/${p}_${method}_RandomizeRsa10-1k.log ]; then
      rm Results/Bacteria/$p/${p}_${method}_RandomizeRsa10-1k.log
    fi
    echo "python3 Scripts/sged-randomize-groups.py \
         --sged-groups=Results/Bacteria/$p/${p}_${method}_significant.tsv \
         --sged-sites=Results/Bacteria/$p/${p}_${method}_siteInfos_PDB_infos.tsv \
         --measure=Rsa \
         --number-replicates=$NSIM \
         --similarity-threshold=0.1 \
         --output=Results/Bacteria/$p/SimulationsRsa/${p}_${method}_random-rsa-10-1k.tsv > Results/Bacteria/$p/${p}_${method}_RandomizeRsa10-1k.log 2>&1" >> cmd-randomize-rsa10-1k.sh
  done
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-randomize-rsa10-1k.sh
```

First add the structural information for each random group:
```bash
rm -f cmd-randomize-rsa10-translate-group-coords.sh

while IFS="" read -r p || [ -n "$p" ]
do
  for method in ind1 ind2 ind3 ind4 ind5 ind6 ind7 ind8 vol pol charge
  do
    echo "python Scripts/sged-translate-coords.py \
           --sged=Results/Bacteria/$p/SimulationsRsa/${p}_${method}_random-rsa-10-1k.tsv \
           --index=Results/Bacteria/$p/${p}_StructureIndex.txt \
           --output=Results/Bacteria/$p/SimulationsRsa/${p}_${method}_random-rsa-10_PDB.tsv \
           --name=PDB >& Results/Bacteria/$p/${p}_${method}_RandomizeRsa10_TranslateGroupCoords.log" >> cmd-randomize-rsa10-translate-group-coords.sh
  done
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-randomize-rsa10-translate-group-coords.sh
```
Check that everything went ok:
```bash
grep Error Results/Bacteria/HOG000*/*RandomizeRsa10_TranslateGroupCoords.log
grep error Results/Bacteria/HOG000*/*RandomizeRsa10_TranslateGroupCoords.log
```

```bash
rm -f cmd-randomize-rsa10-structure-infos.sh

while IFS="" read -r p || [ -n "$p" ]
do
  rm -f Families/Bacteria/$p/*_model0*
  # Get the best PDB structure used for the index:
  PDBFILE=`cat Results/Bacteria/$p/${p}_StructureIndex.txt | grep "# SGED input PDB = " | sed "s/# SGED input PDB = \(.*\)/\1/"`
  PDBCHAIN=`cat Results/Bacteria/$p/${p}_StructureIndex.txt | grep "# SGED input PDB chain = " | sed "s/# SGED input PDB chain = \(.*\)/\1/"`

  for method in ind1 ind2 ind3 ind4 ind5 ind6 ind7 ind8 vol pol charge
  do
    echo "python3 Scripts/sged-structure-infos.py \
         --sged=Results/Bacteria/$p/SimulationsRsa/${p}_${method}_random-rsa-10_PDB.tsv \
         --group=PDB \
         --pdb=$PDBFILE \
         --chain=$PDBCHAIN \
         --measures=AlphaDist,ContactSubgraphs,DSSPsum \
         --output=Results/Bacteria/$p/SimulationsRsa/${p}_${method}_random-rsa-10_PDB_infos.tsv >& Results/Bacteria/$p/${p}_${method}_RandomizeRsa10_GroupStructureInfos.log" >> cmd-randomize-rsa10-structure-infos.sh
  done
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-randomize-rsa10-structure-infos.sh

```

Check that everything went ok:
```bash
grep Error Results/Bacteria/HOG000*/*RandomizeRsa10_GroupStructureInfos.log
grep error Results/Bacteria/HOG000*/*RandomizeRsa10_GroupStructureInfos.log
grep ERROR Results/Bacteria/HOG000*/*RandomizeRsa10_GroupStructureInfos.log
```
=> 3 where DSSP could not be run: HOG000035157, HOG000219148, HOG000231585 (all ribosomal proteins, only Ca available)

Add secondary structure labelling:
```bash
rm -f cmd-randomize-rsa10-structure-infos2.sh

while IFS="" read -r p || [ -n "$p" ]
do
  rm -f Families/Bacteria/$p/*_model0*
  # Get the best PDB structure used for the index:
  PDBID=`cat Results/Bacteria/$p/${p}_StructureIndex.txt | grep "# SGED input PDB = " | sed "s/# SGED input PDB = \(.*\).pdb/\1/"`
  PDBCHAIN=`cat Results/Bacteria/$p/${p}_StructureIndex.txt | grep "# SGED input PDB chain = " | sed "s/# SGED input PDB chain = \(.*\)/\1/"`

  for method in ind1 ind2 ind3 ind4 ind5 ind6 ind7 ind8 vol pol charge
  do
    echo "python3 Scripts/sged-structure-infos.py \
         --sged=Results/Bacteria/$p/SimulationsRsa/${p}_${method}_random-rsa-10_PDB_infos.tsv \
         --group=PDB \
         --pdb=${PDBID}.cif \
         --pdb-format=mmCIF \
         --chain=$PDBCHAIN \
         --measures=SecondaryStructureLabel \
         --output=Results/Bacteria/$p/SimulationsRsa/${p}_${method}_random-rsa-10_PDB_infos2.tsv >& Results/Bacteria/$p/${p}_${method}_RandomizeRsa10_GroupStructureInfos2.log" >> cmd-randomize-rsa10-structure-infos2.sh
  done
done < Families/Bacteria/final_fams_complete.txt

parallel -j 20 --eta < cmd-randomize-rsa10-structure-infos2.sh

```

Check that everything went ok:
```bash
grep Error Results/Bacteria/HOG000*/*RandomizeRsa10_GroupStructureInfos2.log
grep error Results/Bacteria/HOG000*/*RandomizeRsa10_GroupStructureInfos2.log
grep ERROR Results/Bacteria/HOG000*/*RandomizeRsa10_GroupStructureInfos2.log
```


Merge all results into a single file, one per method:
```bash
Rscript Scripts/combineRandomizationAllFamilies.R "SimulationsRsa" "random-rsa-10_PDB_infos" "random-rsa-10_PDB_infos2"
gzip Results/Bacteria/AllFamilies_groups_*_random-rsa-10_PDB_infos.csv
```

## Condition on secondary structure:

Restrict analyses to sites in helices/strands only:

```bash
Rscript Scripts/filterSecondaryStructure.R "HELX_P-" "_helix"
Rscript Scripts/filterSecondaryStructure.R "HELX_P-|NA" "_strand" "invert"
```

Now randomize on the subset of sites:
```bash
function randomize_structure {
  NSIM=1000
  COND_VAR=$1
  SUFFIX_CMD=$2 #Type of analysis, e.g. -rate10-helix
  SUFFIX_LOG=$3 #Type of analysis, e.g. Rate10Helix
  SUFFIX_INF=$4 #File name for site infos, e.g. _helix
  SUFFIX_DIR=$5 #Dir name, e.g. RateHelix
  SIMILARITY=$6

  rm -f cmd-randomize${SUFFIX_CMD}-1k.sh

  while IFS="" read -r p || [ -n "$p" ]
  do
    mkdir Results/Bacteria/$p/Simulations${SUFFIX_DIR}
    for method in ind1 ind2 ind3 ind4 ind5 ind6 ind7 ind8 vol pol charge
    do
      if [ -f Results/Bacteria/$p/${p}_${method}_Randomize${SUFFIX_LOG}-1k.log ]; then
        rm Results/Bacteria/$p/${p}_${method}_Randomize${SUFFIX_LOG}-1k.log
      fi
      if [ -f Results/Bacteria/$p/${p}_${method}_significant_PDB_infos2${SUFFIX_INF}.tsv ]; then
        echo "python3 Scripts/sged-randomize-groups.py \
             --sged-groups=Results/Bacteria/$p/${p}_${method}_significant_PDB_infos2${SUFFIX_INF}.tsv \
             --sged-sites=Results/Bacteria/$p/${p}_${method}_siteInfos_PDB_DisEMBL_infos${SUFFIX_INF}.tsv \
             --measure=$COND_VAR \
             --number-replicates=$NSIM \
             --similarity-threshold=$SIMILARITY \
             --output=Results/Bacteria/$p/Simulations${SUFFIX_DIR}/${p}_${method}_random${SUFFIX_CMD}-1k.tsv > Results/Bacteria/$p/${p}_${method}_Randomize${SUFFIX_LOG}-1k.log 2>&1" \
        >> cmd-randomize${SUFFIX_CMD}-1k.sh
      fi
    done
  done < Families/Bacteria/final_fams_complete.txt
}

randomize_structure "N" "-rate10-helix" "Rate10Helix" "_helix" "RateHelix" 0.1
parallel -j 20 --eta < cmd-randomize-rate10-helix-1k.sh

randomize_structure "N" "-rate10-strand" "Rate10Strand" "_strand" "RateStrand" 0.1
parallel -j 20 --eta < cmd-randomize-rate10-strand-1k.sh

randomize_structure "Rsa" "-rsa10-helix" "Rsa10Helix" "_helix" "RsaHelix" 0.1
parallel -j 20 --eta < cmd-randomize-rsa10-helix-1k.sh

randomize_structure "Rsa" "-rsa10-strand" "Rsa10Strand" "_strand" "RsaStrand" 0.1
parallel -j 20 --eta < cmd-randomize-rsa10-strand-1k.sh

randomize_structure "N" "-helix" "Helix" "_helix" "Helix" 100000
parallel -j 20 --eta < cmd-randomize-helix-1k.sh

randomize_structure "N" "-strand" "Strand" "_strand" "Strand" 100000
parallel -j 20 --eta < cmd-randomize-strand-1k.sh


```

Add the structural information for each random group:
```bash
function translate_randomized {
  SUFFIX_CMD=$1 #Type of analysis, e.g. -rate10-helix
  SUFFIX_LOG=$2 #Type of analysis, e.g. Rate10Helix
  SUFFIX_INF=$3 #File name for site infos, e.g. _helix
  SUFFIX_DIR=$4 #Dir name, e.g. RateHelix


  rm -f cmd-randomize${SUFFIX_CMD}-translate-group-coords.sh

  while IFS="" read -r p || [ -n "$p" ]
  do
    for method in ind1 ind2 ind3 ind4 ind5 ind6 ind7 ind8 vol pol charge
    do
      if [ -f Results/Bacteria/$p/${p}_${method}_Randomize${SUFFIX_LOG}_TranslateGroupCoords.log ]; then
        rm Results/Bacteria/$p/${p}_${method}_Randomize${SUFFIX_LOG}_TranslateGroupCoords.log
      fi
      if [ -f Results/Bacteria/$p/${p}_${method}_significant_PDB_infos2${SUFFIX_INF}.tsv ]; then
        echo "python Scripts/sged-translate-coords.py \
               --sged=Results/Bacteria/$p/Simulations${SUFFIX_DIR}/${p}_${method}_random${SUFFIX_CMD}-1k.tsv \
               --index=Results/Bacteria/$p/${p}_StructureIndex.txt \
               --output=Results/Bacteria/$p/Simulations${SUFFIX_DIR}/${p}_${method}_random${SUFFIX_CMD}_PDB.tsv \
               --name=PDB >& Results/Bacteria/$p/${p}_${method}_Randomize${SUFFIX_LOG}_TranslateGroupCoords.log" \
               >> cmd-randomize${SUFFIX_CMD}-translate-group-coords.sh
      fi
    done
  done < Families/Bacteria/final_fams_complete.txt
}

translate_randomized "-rate10-helix" "Rate10Helix" "_helix" "RateHelix"
parallel -j 20 --eta < cmd-randomize-rate10-helix-translate-group-coords.sh

translate_randomized "-rate10-strand" "Rate10Strand" "_strand" "RateStrand"
parallel -j 20 --eta < cmd-randomize-rate10-strand-translate-group-coords.sh

translate_randomized "-rsa10-helix" "Rsa10Helix" "_helix" "RsaHelix"
parallel -j 20 --eta < cmd-randomize-rsa10-helix-translate-group-coords.sh

translate_randomized "-rsa10-strand" "Rsa10Strand" "_strand" "RsaStrand"
parallel -j 20 --eta < cmd-randomize-rsa10-strand-translate-group-coords.sh

translate_randomized "-helix" "Helix" "_helix" "Helix"
parallel -j 20 --eta < cmd-randomize-helix-translate-group-coords.sh

translate_randomized "-strand" "Strand" "_strand" "Strand"
parallel -j 20 --eta < cmd-randomize-strand-translate-group-coords.sh

```

Check that everything went ok:
```bash
grep Error Results/Bacteria/HOG000*/*RandomizeRate10Helix_TranslateGroupCoords.log
grep error Results/Bacteria/HOG000*/*RandomizeRate10Helix_TranslateGroupCoords.log
grep Error Results/Bacteria/HOG000*/*RandomizeRate10Strand_TranslateGroupCoords.log
grep error Results/Bacteria/HOG000*/*RandomizeRate10Strand_TranslateGroupCoords.log
grep Error Results/Bacteria/HOG000*/*RandomizeRsa10Helix_TranslateGroupCoords.log
grep error Results/Bacteria/HOG000*/*RandomizeRsa10Helix_TranslateGroupCoords.log
grep Error Results/Bacteria/HOG000*/*RandomizeRsa10Strand_TranslateGroupCoords.log
grep error Results/Bacteria/HOG000*/*RandomizeRsa10Strand_TranslateGroupCoords.log
grep Error Results/Bacteria/HOG000*/*RandomizeHelix_TranslateGroupCoords.log
grep error Results/Bacteria/HOG000*/*RandomizeHelix_TranslateGroupCoords.log
grep Error Results/Bacteria/HOG000*/*RandomizeStrand_TranslateGroupCoords.log
grep error Results/Bacteria/HOG000*/*RandomizeStrand_TranslateGroupCoords.log
```

```bash
function infos_randomized {
  SUFFIX_CMD=$1 #Type of analysis, e.g. -rate10-helix
  SUFFIX_LOG=$2 #Type of analysis, e.g. Rate10Helix
  SUFFIX_INF=$3 #File name for site infos, e.g. _helix
  SUFFIX_DIR=$4 #Dir name, e.g. RateHelix

  rm -f cmd-randomize${SUFFIX_CMD}-structure-infos.sh

  while IFS="" read -r p || [ -n "$p" ]
  do
    rm -f Families/Bacteria/$p/*_model0*
    # Get the best PDB structure used for the index:
    PDBFILE=`cat Results/Bacteria/$p/${p}_StructureIndex.txt | grep "# SGED input PDB = " | sed "s/# SGED input PDB = \(.*\)/\1/"`
    PDBCHAIN=`cat Results/Bacteria/$p/${p}_StructureIndex.txt | grep "# SGED input PDB chain = " | sed "s/# SGED input PDB chain = \(.*\)/\1/"`

    for method in ind1 ind2 ind3 ind4 ind5 ind6 ind7 ind8 vol pol charge
    do
      if [ -f Results/Bacteria/$p/${p}_${method}_Randomize${SUFFIX_LOG}_GroupStructureInfos.log ]; then
        rm Results/Bacteria/$p/${p}_${method}_Randomize${SUFFIX_LOG}_GroupStructureInfos.log
      fi
      if [ -f Results/Bacteria/$p/${p}_${method}_significant_PDB_infos2${SUFFIX_INF}.tsv ]; then
        echo "python3 Scripts/sged-structure-infos.py \
             --sged=Results/Bacteria/$p/Simulations${SUFFIX_DIR}/${p}_${method}_random${SUFFIX_CMD}_PDB.tsv \
             --group=PDB \
             --pdb=$PDBFILE \
             --chain=$PDBCHAIN \
             --measures=AlphaDist,ContactSubgraphs,DSSPsum \
             --output=Results/Bacteria/$p/Simulations${SUFFIX_DIR}/${p}_${method}_random${SUFFIX_CMD}_PDB_infos.tsv >& Results/Bacteria/$p/${p}_${method}_Randomize${SUFFIX_LOG}_GroupStructureInfos.log" \
             >> cmd-randomize${SUFFIX_CMD}-structure-infos.sh
      fi
    done
  done < Families/Bacteria/final_fams_complete.txt
}

infos_randomized "-rate10-helix" "Rate10Helix" "_helix" "RateHelix"
parallel -j 20 --eta < cmd-randomize-rate10-helix-structure-infos.sh

infos_randomized "-rate10-strand" "Rate10Strand" "_strand" "RateStrand"
parallel -j 20 --eta < cmd-randomize-rate10-strand-structure-infos.sh

infos_randomized "-rsa10-helix" "Rsa10Helix" "_helix" "RsaHelix"
parallel -j 20 --eta < cmd-randomize-rsa10-helix-structure-infos.sh

infos_randomized "-rsa10-strand" "Rsa10Strand" "_strand" "RsaStrand"
parallel -j 20 --eta < cmd-randomize-rsa10-strand-structure-infos.sh

infos_randomized "-helix" "Helix" "_helix" "Helix"
parallel -j 20 --eta < cmd-randomize-helix-structure-infos.sh

infos_randomized "-strand" "Strand" "_strand" "Strand"
parallel -j 20 --eta < cmd-randomize-strand-structure-infos.sh

```

Check that everything went ok:
```bash
grep Error Results/Bacteria/HOG000*/*RandomizeRate10Helix_GroupStructureInfos.log
grep error Results/Bacteria/HOG000*/*RandomizeRate10Helix_GroupStructureInfos.log
grep ERROR Results/Bacteria/HOG000*/*RandomizeRate10Helix_GroupStructureInfos.log

grep Error Results/Bacteria/HOG000*/*RandomizeRate10Strand_GroupStructureInfos.log
grep error Results/Bacteria/HOG000*/*RandomizeRate10Strand_GroupStructureInfos.log
grep ERROR Results/Bacteria/HOG000*/*RandomizeRate10Strand_GroupStructureInfos.log

grep Error Results/Bacteria/HOG000*/*RandomizeRsa10Helix_GroupStructureInfos.log
grep error Results/Bacteria/HOG000*/*RandomizeRsa10Helix_GroupStructureInfos.log
grep ERROR Results/Bacteria/HOG000*/*RandomizeRsa10Helix_GroupStructureInfos.log

grep Error Results/Bacteria/HOG000*/*RandomizeRsa10Strand_GroupStructureInfos.log
grep error Results/Bacteria/HOG000*/*RandomizeRsa10Strand_GroupStructureInfos.log
grep ERROR Results/Bacteria/HOG000*/*RandomizeRsa10Strand_GroupStructureInfos.log

grep Error Results/Bacteria/HOG000*/*RandomizeHelix_GroupStructureInfos.log
grep error Results/Bacteria/HOG000*/*RandomizeHelix_GroupStructureInfos.log
grep ERROR Results/Bacteria/HOG000*/*RandomizeHelix_GroupStructureInfos.log

grep Error Results/Bacteria/HOG000*/*RandomizeStrand_GroupStructureInfos.log
grep error Results/Bacteria/HOG000*/*RandomizeStrand_GroupStructureInfos.log
grep ERROR Results/Bacteria/HOG000*/*RandomizeStrand_GroupStructureInfos.log
```


Add secondary structure labelling:
```bash
function infos2_randomized {
  SUFFIX_CMD=$1 #Type of analysis, e.g. -rate10-helix
  SUFFIX_LOG=$2 #Type of analysis, e.g. Rate10Helix
  SUFFIX_INF=$3 #File name for site infos, e.g. _helix
  SUFFIX_DIR=$4 #Dir name, e.g. RateHelix

  rm -f cmd-randomize${SUFFIX_CMD}-infos2.sh

  while IFS="" read -r p || [ -n "$p" ]
  do
    rm -f Families/Bacteria/$p/*_model0*
    # Get the best PDB structure used for the index:
    PDBID=`cat Results/Bacteria/$p/${p}_StructureIndex.txt | grep "# SGED input PDB = " | sed "s/# SGED input PDB = \(.*\).pdb/\1/"`
    PDBCHAIN=`cat Results/Bacteria/$p/${p}_StructureIndex.txt | grep "# SGED input PDB chain = " | sed "s/# SGED input PDB chain = \(.*\)/\1/"`

    for method in ind1 ind2 ind3 ind4 ind5 ind6 ind7 ind8 vol pol charge
    do
      if [ -f Results/Bacteria/$p/${p}_${method}_Randomize${SUFFIX_LOG}_GroupStructureInfos2.log ]; then
        rm Results/Bacteria/$p/${p}_${method}_Randomize${SUFFIX_LOG}_GroupStructureInfos2.log
      fi
      if [ -f Results/Bacteria/$p/${p}_${method}_significant_PDB_infos2${SUFFIX_INF}.tsv ]; then  
        echo "python3 Scripts/sged-structure-infos.py \
             --sged=Results/Bacteria/$p/Simulations${SUFFIX_DIR}/${p}_${method}_random${SUFFIX_CMD}_PDB_infos.tsv \
             --group=PDB \
             --pdb=${PDBID}.cif \
             --pdb-format=mmCIF \
             --chain=$PDBCHAIN \
             --measures=SecondaryStructureLabel \
             --output=Results/Bacteria/$p/Simulations${SUFFIX_DIR}/${p}_${method}_random${SUFFIX_CMD}_PDB_infos2.tsv >& Results/Bacteria/$p/${p}_${method}_Randomize${SUFFIX_LOG}_GroupStructureInfos2.log" \
             >> cmd-randomize${SUFFIX_CMD}-structure-infos2.sh
      fi
    done
  done < Families/Bacteria/final_fams_complete.txt
}

infos2_randomized "-rate10-helix" "Rate10Helix" "_helix" "RateHelix"
parallel -j 20 --eta < cmd-randomize-rate10-helix-structure-infos2.sh

infos2_randomized "-rate10-strand" "Rate10Strand" "_strand" "RateStrand"
parallel -j 20 --eta < cmd-randomize-rate10-strand-structure-infos2.sh

infos2_randomized "-rsa10-helix" "Rsa10Helix" "_helix" "RsaHelix"
parallel -j 20 --eta < cmd-randomize-rsa10-helix-structure-infos2.sh

infos2_randomized "-rsa10-strand" "Rsa10Strand" "_strand" "RsaStrand"
parallel -j 20 --eta < cmd-randomize-rsa10-strand-structure-infos2.sh

infos2_randomized "-helix" "Helix" "_helix" "Helix"
parallel -j 20 --eta < cmd-randomize-helix-structure-infos2.sh

infos2_randomized "-strand" "Strand" "_strand" "Strand"
parallel -j 20 --eta < cmd-randomize-strand-structure-infos2.sh

```

Check that everything went ok:
```bash
grep Error Results/Bacteria/HOG000*/*RandomizeRate10Helix_GroupStructureInfos2.log
grep error Results/Bacteria/HOG000*/*RandomizeRate10Helix_GroupStructureInfos2.log
grep ERROR Results/Bacteria/HOG000*/*RandomizeRate10Helix_GroupStructureInfos2.log

grep Error Results/Bacteria/HOG000*/*RandomizeRate10Strand_GroupStructureInfos2.log
grep error Results/Bacteria/HOG000*/*RandomizeRate10Strand_GroupStructureInfos2.log
grep ERROR Results/Bacteria/HOG000*/*RandomizeRate10Strand_GroupStructureInfos2.log

grep Error Results/Bacteria/HOG000*/*RandomizeRsa10Helix_GroupStructureInfos2.log
grep error Results/Bacteria/HOG000*/*RandomizeRsa10Helix_GroupStructureInfos2.log
grep ERROR Results/Bacteria/HOG000*/*RandomizeRsa10Helix_GroupStructureInfos2.log

grep Error Results/Bacteria/HOG000*/*RandomizeRsa10Strand_GroupStructureInfos2.log
grep error Results/Bacteria/HOG000*/*RandomizeRsa10Strand_GroupStructureInfos2.log
grep ERROR Results/Bacteria/HOG000*/*RandomizeRsa10Strand_GroupStructureInfos2.log

grep Error Results/Bacteria/HOG000*/*RandomizeHelix_GroupStructureInfos2.log
grep error Results/Bacteria/HOG000*/*RandomizeHelix_GroupStructureInfos2.log
grep ERROR Results/Bacteria/HOG000*/*RandomizeHelix_GroupStructureInfos2.log

grep Error Results/Bacteria/HOG000*/*RandomizeStrand_GroupStructureInfos2.log
grep error Results/Bacteria/HOG000*/*RandomizeStrand_GroupStructureInfos2.log
grep ERROR Results/Bacteria/HOG000*/*RandomizeStrand_GroupStructureInfos2.log

```


Merge all results into a single file, one per method:
```bash
Rscript Scripts/combineRandomizationAllFamilies.R "SimulationsRateHelix" "random-rate10-helix_PDB_infos" "random-rate10-helix_PDB_infos2"
gzip Results/Bacteria/AllFamilies_groups_*_random-rate10-helix_PDB_infos.csv

Rscript Scripts/combineRandomizationAllFamilies.R "SimulationsRateStrand" "random-rate10-strand_PDB_infos" "random-rate10-strand_PDB_infos2"
gzip Results/Bacteria/AllFamilies_groups_*_random-rate10-strand_PDB_infos.csv

Rscript Scripts/combineRandomizationAllFamilies.R "SimulationsRsaHelix" "random-rsa10-helix_PDB_infos" "random-rsa10-helix_PDB_infos2"
gzip Results/Bacteria/AllFamilies_groups_*_random-rsa10-helix_PDB_infos.csv

Rscript Scripts/combineRandomizationAllFamilies.R "SimulationsRsaStrand" "random-rsa10-strand_PDB_infos" "random-rsa10-strand_PDB_infos2"
gzip Results/Bacteria/AllFamilies_groups_*_random-rsa10-strand_PDB_infos.csv

Rscript Scripts/combineRandomizationAllFamilies.R "SimulationsHelix" "random-helix_PDB_infos" "random-helix_PDB_infos2"
gzip Results/Bacteria/AllFamilies_groups_*_random-helix_PDB_infos.csv

Rscript Scripts/combineRandomizationAllFamilies.R "SimulationsStrand" "random-strand_PDB_infos" "random-strand_PDB_infos2"
gzip Results/Bacteria/AllFamilies_groups_*_random-strand_PDB_infos.csv

```


Prepare table describing all families
=====================================


```r
require(ape)
files <- list.files(path = "Families/Bacteria/", pattern = "*_sample_2.phy_phyml_tree.txt$", recursive = TRUE)
# Read from family list instead

n <- length(files)
x.nbseq <- numeric(n)
x.nbsit <- numeric(n)
x.famil <- character(n)
x.pdbst <- character(n)
for (i in 1:n) {
  l <- strsplit(files[i], "/")
  fam <- l[[1]][1]
  cat(fam, "\n")
  
  tree <- read.tree(paste0("Families/Bacteria/", files[i]))
  nb.sequences <- length(tree$tip.label)
  
  sites <- read.table(paste0("Families/Bacteria/", fam, "/", fam, "_vol_siteInfos.csv"), header = TRUE)
  nb.sites <- sum(sites$IsComplete)

  tmp1 <- readLines(paste0("Results/Bacteria/", fam, "/", fam, "_StructureIndex.txt"))[4]
  tmp2 <- readLines(paste0("Results/Bacteria/", fam, "/", fam, "_StructureIndex.txt"))[5]
  
  tmp1 <- strsplit(tmp1, "/")[[1]]
  tmp1 <- tmp1[length(tmp1)]
  tmp1 <- strsplit(tmp1, "\\.")[[1]]

  tmp2 <- strsplit(tmp2, "= ")[[1]]
  tmp2 <- tmp2[length(tmp2)]
 
  pdb <- paste(tmp1[1], tmp2, sep = "-")

  x.famil[i] <- fam
  x.pdbst[i] <- pdb
  x.nbseq[i] <- nb.sequences
  x.nbsit[i] <- nb.sites
}

res <- data.frame(Family = x.famil, PDB = x.pdbst, NbSequences = x.nbseq, NbSites = x.nbsit)
write.csv(res, file = "FamilyProperties.csv", row.names = FALSE, quote = FALSE)
```

