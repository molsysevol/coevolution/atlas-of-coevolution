# Created on 23/03/2021 by jdutheil

require(data.table)
require(plyr)
require(ggplot2)
require(ggpubr)
require(scales)
require(cowplot)

methods <- c("ind5", "ind6", "ind7", "ind8", "ind1", "ind2", "ind3", "ind4", "vol", "pol", "charge")
labels <- c("I5", "I6", "I7", "I8", "I1", "I2", "I3", "I4", "Volume", "Polarity", "Charge")

# Double-sided sqrt transform:
sym_sqrt_trans <- function() trans_new("sym_sqrt",
                                       transform = function (x) ( sign(x)*sqrt(abs(x)) ),
                                       inverse = function (y) ( sign(y)*(abs(y) * abs(y)) ),
                                       domain = c(-Inf, Inf))

#### Conditioning on rates ####

read.sim.rates <- function(method) {
  dat.rndNo <- read.table(paste("AllFamilies_groups_", method, "_random.csv.gz", sep = ""), header = TRUE, sep = ",")
  dat.rnd10 <- read.table(paste("AllFamilies_groups_", method, "_random-rates-10.csv.gz", sep = ""), header = TRUE, sep = ",")
  dat.rnd20 <- read.table(paste("AllFamilies_groups_", method, "_random-rates-20.csv.gz", sep = ""), header = TRUE, sep = ",")
  dat.rndNo$Threshold <- "None"
  dat.rnd10$Threshold <- "10%"
  dat.rnd20$Threshold <- "20%"
  dat.rnd <- rbind(dat.rndNo, dat.rnd10, dat.rnd20)
  dat.rnd$Threshold <- ordered(dat.rnd$Threshold, levels = c("None", "20%", "10%"))
  dat.rnd$Method <- method
  return(dat.rnd)
}

l <- list()
for (method in methods) {
  cat("Method:", method, "\n")
  l[[method]] <- read.sim.rates(method)
}
dat.rates <- rbindlist(l)
dat.rates$Method <- ordered(dat.rates$Method, levels = methods, labels = labels)

p.rate <- ggplot(data = dat.rates, aes(y = (OrigMean - RandMean)/(OrigMean + RandMean), x = Threshold)) +
  geom_boxplot() + facet_wrap(~Method, as.table = FALSE) + 
  scale_y_continuous(trans = "sym_sqrt", breaks = c(-1, -0.5, -0.2, 0, 0.2, 0.5, 1.0)) + 
  ylab("Relative mean rate difference") + theme_pubr() +
  theme(panel.grid.major.y = element_line())
p.rate

#### Conditioning on RSA ####

read.sim.rsa <- function(method) {
  dat.rndNo <- read.table(paste("AllFamilies_groups_", method, "_random-rsa.csv.gz", sep = ""), header = TRUE, sep = ",")
  dat.rnd10 <- read.table(paste("AllFamilies_groups_", method, "_random-rsa-10.csv.gz", sep = ""), header = TRUE, sep = ",")
  dat.rnd20 <- read.table(paste("AllFamilies_groups_", method, "_random-rsa-20.csv.gz", sep = ""), header = TRUE, sep = ",")
  dat.rndNo$Threshold <- "None"
  dat.rnd10$Threshold <- "10%"
  dat.rnd20$Threshold <- "20%"
  dat.rnd <- rbind(dat.rndNo, dat.rnd10, dat.rnd20)
  dat.rnd$Threshold <- ordered(dat.rnd$Threshold, levels = c("None", "20%", "10%"))
  dat.rnd$Method <- method
  return(dat.rnd)
}

l <- list()
for (method in methods) {
  cat("Method:", method, "\n")
  l[[method]] <- read.sim.rsa(method)
}
dat.rsa <- rbindlist(l)
dat.rsa$Method <- ordered(dat.rsa$Method, levels = methods, labels = labels)


p.rsa <- ggplot(data = dat.rsa, aes(y = (OrigMean - RandMean)/(OrigMean + RandMean), x = Threshold)) +
  geom_boxplot() + facet_wrap(~Method, as.table = FALSE) + 
  scale_y_continuous(trans = "sym_sqrt", breaks = c(-1, -0.5, -0.2, 0, 0.2, 0.5, 1.0)) + 
  ylab("Relative mean RSA difference") + theme_pubr() + 
  theme(panel.grid.major.y = element_line())
p.rsa

# Create a supplementary figure:
p <- plot_grid(p.rate, p.rsa, labels = "AUTO", nrow = 1)

ggsave(p, filename = "ConditionalRandomization.pdf", width = 14, height = 7)


#### Significance of 3D statistics ####

get.secondary.structure.stats <- function(dat) {
  s <- substring(dat$SecondaryStructureLabels, first = 2, last = nchar(dat$SecondaryStructureLabels) - 1)
  lst <- strsplit(s, ";")
  lst <- lapply(lst, function(x) x[x != "NA"]) #Keep only secondary structure elements
  dat$SecondaryStructure.NbMapped <- sapply(lst, length)
  dat$SecondaryStructure.NbSubgraphs <- sapply(lst, function(x) length(unique(x)))
  lst.hlx <- lapply(lst, function(x) {
    sublst <- strsplit(x, "-")
    y <- sapply(sublst, function(xx) ifelse(xx[1] == "HELX_P", xx[2], NA))
    return(y[!is.na(y)])
  })
  dat$Helix.NbMapped <- sapply(lst.hlx, length)
  dat$Helix.NbSubgraphs <- sapply(lst.hlx, function(x) {
      n <- length(unique(x))
      ifelse(n == 0, NA, n)
    })
  lst.str <- lapply(lst, function(x) {
    sublst <- strsplit(x, "-")
    y <- sapply(sublst, function(xx) ifelse(xx[1] != "HELX_P", xx[2], NA))
    return(y[!is.na(y)])
  })
  dat$Strand.NbMapped <- sapply(lst.str, length)
  dat$Strand.NbSubgraphs <- sapply(lst.str, function(x) {
      n <- length(unique(x))
      ifelse(n == 0, NA, n)
    })
  lst.sht <- lapply(lst, function(x) {
    sublst <- strsplit(x, "-")
    y <- sapply(sublst, function(xx) ifelse(xx[1] != "HELX_P", xx[1], NA))
    return(y[!is.na(y)])
  })
  # Note: nb of positions mapped is the same as for strands.
  dat$Sheet.NbSubgraphs <- sapply(lst.sht, function(x) {
      n <- length(unique(x))
      ifelse(n == 0, NA, n)
    })
  return(dat)
}

get.perm.data <- function(type) {
  l <- list()
  idx <- list()
  for (method in methods) {
    cat("Method:", method, "\n")
    dat.rnd <- read.table(paste("AllFamilies_groups_", method, "_random-", type, "_PDB_infos.csv.gz", sep = ""), sep = ",", header = TRUE)
    # Secondary structure analysis:
    dat.rnd <- get.secondary.structure.stats(dat.rnd)
    # Get the index of positions that could be analysed:
    nr <- length(unique(dat.rnd$Replicate))
    ng <- nrow(dat.rnd) / nr
    dat.rnd$GroupOrigId <- rep(1:ng, each = nr)
    dat.idx <- ddply(dat.rnd, .variables = "GroupOrigId", function(d) {
      return(c("IsComplete" = length(grep("NA", d$Group)) == 0))
    })
    dat.idx$Method <- method
    idx[[method]] <- dat.idx
    # Only keep complete groups:
    dat.rnd <- subset(dat.rnd, GroupOrigId %in% dat.idx$GroupOrigId[dat.idx$IsComplete])
    
    # Now compute average per replicate:
    l[[method]] <- ddply(dat.rnd, .variables = "Replicate", plyr::summarize, 
                         Size = mean(Size, na.rm = TRUE),
                         MeanNbPositionsMapped = mean(ContactSubgraphs.NbMapped, na.rm = TRUE),
                         MeanAlphaDist = mean(AlphaDistMax, na.rm = TRUE),
                         MeanNbSubgraphs = mean((ContactSubgraphs.NbSubgraphs - 1)/(ContactSubgraphs.NbMapped - 1), na.rm = TRUE),
                         MeanNbPositionsSecondaryStructure = mean(SecondaryStructure.NbMapped),
                         MeanNbPositionsHelix = mean(Helix.NbMapped),
                         MeanNbPositionsStrand = mean(Strand.NbMapped),
                         MeanNbSubgraphsSecondaryStructure = mean((SecondaryStructure.NbSubgraphs - 1)/(SecondaryStructure.NbMapped - 1), na.rm = TRUE),
                         MeanNbSubgraphsHelix = mean((Helix.NbSubgraphs - 1)/(Helix.NbMapped - 1), na.rm = TRUE),
                         MeanNbSubgraphsStrand = mean((Strand.NbSubgraphs - 1)/(Strand.NbMapped - 1), na.rm = TRUE),
                         MeanNbSubgraphsSheet = mean((Sheet.NbSubgraphs - 1)/(Strand.NbMapped - 1), na.rm = TRUE),
                         PropPositionsSecondaryStructure = sum(SecondaryStructure.NbMapped, na.rm = TRUE) / sum(ContactSubgraphs.NbMapped, na.rm = TRUE),
                         PropPositionsHelix = sum(Helix.NbMapped, na.rm = TRUE) / sum(ContactSubgraphs.NbMapped, na.rm = TRUE),
                         PropPositionsStrand = sum(Strand.NbMapped, na.rm = TRUE) / sum(ContactSubgraphs.NbMapped, na.rm = TRUE),
                         Method = unique(Method))
  }
  dat.rnd.sum <- as.data.frame(rbindlist(l))
  dat.rnd.sum$Method <- ordered(dat.rnd.sum$Method, levels = methods, labels = labels)
  dat.rnd.idx <- rbindlist(idx)
  return(list(data = dat.rnd.sum, index = dat.rnd.idx))
}

# Conditioned on rates:
perm.rates <- get.perm.data("rates-10")
dat.rnd.rates <- perm.rates$data
dat.rnd.rates.index <- perm.rates$index

# Conditioned on RSA:
perm.rsa <- get.perm.data("rsa-10")
dat.rnd.rsa <- perm.rsa$data
dat.rnd.rsa.index <- perm.rsa$index

# merge the two:
dat.rnd.rates$Condition <- "Rates"
dat.rnd.rsa$Condition <- "RSA"
dat.rnd <- as.data.frame(rbind(dat.rnd.rates, dat.rnd.rsa))

write.csv(dat.rnd, file = "RandomizedData.csv", row.names = FALSE)

# Now get the observed statistics, restricting to groups for which random sites could be generated:
dat.obs <- read.table("../AllFamilies_groups_molted.csv.gz", sep = ",", header = TRUE)
dat.obs <- subset(dat.obs, Method %in% methods)
dat.obs <- get.secondary.structure.stats(dat.obs)

# Restrict to randomizable groups:
filter.observed.data <- function(dat.obs, index) {
  l <- list()
  for (method in methods) {
    idx <- subset(index, Method == method)
    tmp <- subset(dat.obs, Method == method)
    n1 <- nrow(tmp)
    tmp <- tmp[idx$IsComplete,] # Assumes the order of groups has been kept
    n2 <- nrow(tmp)
    cat("Removed ", (n1 - n2)*100/n1, "% groups for method ", method, ".\n", sep = "")
    l[[method]] <- tmp
  }
  dat.obs.filtered <- as.data.frame(rbindlist(l))
  n1 <- nrow(dat.obs)
  n2 <- nrow(dat.obs.filtered)
  cat((n1-n2)*100/n1, "% groups filtered out.\n", sep = "")
  return(dat.obs.filtered)
}

dat.obs.rates <- filter.observed.data(dat.obs, dat.rnd.rates.index) #0.3542793% groups filtered out.
dat.obs.rsa   <- filter.observed.data(dat.obs, dat.rnd.rsa.index) #30.58456% groups filtered out.

summarise.observed.data <- function(dat.obs) {
  dat.obs.sum <- ddply(
    dat.obs, .variables = "Method", plyr::summarize,
    Size = mean(Size, na.rm = TRUE),
    MeanNbPositionsMapped = mean(ContactSubgraphs.NbMapped, na.rm = TRUE),
    MeanAlphaDist = mean(AlphaDistMax, na.rm = TRUE),
    MeanNbSubgraphs = mean((ContactSubgraphs.NbSubgraphs - 1)/(ContactSubgraphs.NbMapped - 1), na.rm = TRUE),
    MeanNbPositionsSecondaryStructure = mean(SecondaryStructure.NbMapped),
    MeanNbPositionsHelix = mean(Helix.NbMapped),
    MeanNbPositionsStrand = mean(Strand.NbMapped),
    MeanNbSubgraphsSecondaryStructure = mean((SecondaryStructure.NbSubgraphs - 1)/(SecondaryStructure.NbMapped - 1), na.rm = TRUE),
    MeanNbSubgraphsHelix = mean((Helix.NbSubgraphs - 1)/(Helix.NbMapped - 1), na.rm = TRUE),
    MeanNbSubgraphsStrand = mean((Strand.NbSubgraphs - 1)/(Strand.NbMapped - 1), na.rm = TRUE),
    MeanNbSubgraphsSheet = mean((Sheet.NbSubgraphs - 1)/(Strand.NbMapped - 1), na.rm = TRUE),
    PropPositionsSecondaryStructure = sum(SecondaryStructure.NbMapped, na.rm = TRUE) / sum(ContactSubgraphs.NbMapped, na.rm = TRUE),
    PropPositionsHelix = sum(Helix.NbMapped, na.rm = TRUE) / sum(ContactSubgraphs.NbMapped, na.rm = TRUE),
    PropPositionsStrand = sum(Strand.NbMapped, na.rm = TRUE) / sum(ContactSubgraphs.NbMapped, na.rm = TRUE))
  dat.obs.sum$Method <- ordered(dat.obs.sum$Method, levels = methods, labels = labels)
  return(dat.obs.sum)
}

dat.obs.rates.sum <- summarise.observed.data(dat.obs.rates)
dat.obs.rsa.sum   <- summarise.observed.data(dat.obs.rsa)

# merge the two:
dat.obs.rates.sum$Condition <- "Rates"
dat.obs.rsa.sum$Condition <- "RSA"
dat.obs.sum <- as.data.frame(rbind(dat.obs.rates.sum, dat.obs.rsa.sum))


# Compute P values:

# From gtools:
stars.pval <- function(p.value) {
  unclass(
    symnum(p.value, corr = FALSE, na = FALSE,
           cutpoints = c(0, 0.001, 0.01, 0.05, 0.1, 1),
           symbols = c("(***)", "(**)", "(*)", "(.)", ""))
  )
}

compute.p.values <- function(stat, condition, test = "lower", .dat.rnd = dat.rnd, .dat.obs.sum = dat.obs.sum) {
  res <- ddply(subset(.dat.obs.sum, Condition == condition),
               .variables = c("Method"),
               .fun = function(d) {
    s <- d[, stat]
    m <- unique(d$Method)
    sim <- subset(.dat.rnd, Condition == condition & Method == m)
    x <- unlist(sim[, stat])
    p1 <- (sum(x <= s) + 1) / (nrow(sim) + 1)
    p2 <- (sum(x >= s) + 1) / (nrow(sim) + 1)
    if (test == "lower") p <- p1
    else if (test == "greater") p <- p2
    else if (test == "both") p <- min(1, min(p1, p2) * 2)
    else p <- NA
    return(data.frame(a=mean(x), b = s, c=p, d=stars.pval(p)))
  })
  names(res) <- c("Method",
                  paste(stat, ".simmean", sep = ""),
                  paste(stat, ".obsval", sep = ""),
                  paste(stat, ".pvalue", sep = ""),
                  paste(stat, ".psignif", sep = ""))
  res$Condition <- condition
  return(res)
}

dat.dist.pvalues <- rbind(
  compute.p.values("MeanAlphaDist", "Rates", "lower"),
  compute.p.values("MeanAlphaDist", "RSA", "lower"))
ymax <- 3
dat.dist.pvalues$y <- rep(c(0.9, 0.7) * ymax, each = 11)
# All P values highly significant (lowest possible value). We do not add them on the graph

p.dist <- ggdensity(dat.rnd, x = "MeanAlphaDist", fill = "Condition") +
  scale_fill_manual(values = c("Rates" = "black", "RSA" = "grey")) +
  scale_x_continuous(guide = guide_axis(n.dodge = 2)) + 
  #scale_x_continuous(guide = guide_axis(n.dodge = 2), limits = c(20, 42)) + 
  #ylim(0, ymax) +
  geom_vline(data = dat.obs.sum, aes(xintercept = MeanAlphaDist, col = Condition)) +
  scale_color_manual(values = c("Rates" = "black", "RSA" = "grey")) +
  #geom_text(data = dat.dist.pvalues, aes(x = MeanAlphaDist.simmean,
  #                                       y = y,
  #                                       color = Condition,
  #                                       label = paste(sprintf("%.2f", MeanAlphaDist.pvalue), " ", MeanAlphaDist.psignif, sep = "")),
  #          hjust = 0, size = 3) +
  facet_wrap(~Method, as.table = FALSE) + theme_pubclean(flip = TRUE) +
  xlab(expression(paste("Mean C", alpha, " distance (Å)"))) + ylab("Density")
p.dist

dat.subg.pvalues <- rbind(
  compute.p.values("MeanNbSubgraphs", "Rates", "lower"),
  compute.p.values("MeanNbSubgraphs", "RSA", "lower"))
ymax <- 150
dat.subg.pvalues$y <- rep(c(0.9, 0.7) * ymax, each = 11)

p.subg <- ggdensity(dat.rnd, x = "MeanNbSubgraphs", fill = "Condition") +
  scale_fill_manual(values = c("Rates" = "black", "RSA" = "grey")) +
  #scale_x_continuous(guide = guide_axis(n.dodge = 2), limits = c(0.6, 1.1), breaks = c(0.6, 0.7, 0.8, 0.9, 1.0)) + ylim(0, ymax) +
  scale_x_continuous(guide = guide_axis(n.dodge = 2)) +
  #ylim(0, ymax) +
  geom_vline(data = dat.obs.sum, aes(xintercept = MeanNbSubgraphs, col = Condition)) +
  scale_color_manual(values = c("Rates" = "black", "RSA" = "grey")) +
  #geom_text(data = dat.subg.pvalues, aes(x = MeanNbSubgraphs.simmean,
  #                                       y = y,
  #                                       color = Condition,
  #                                       label = paste(sprintf("%.2f", MeanNbSubgraphs.pvalue), " ", MeanNbSubgraphs.psignif, sep = "")),
  #          hjust = 0, size = 3) +
  facet_wrap(~Method, as.table = FALSE) + theme_pubclean(flip = TRUE) +
  xlab("(Number of graphs - 1) / (Group size - 1)") + ylab("Density")
p.subg

p <- plot_grid(p.dist, p.subg, labels = "AUTO", nrow = 1)
p

ggsave(p, filename = "3dDistance.pdf", width = 12, height = 6)

# Secondary structure:

dat.secstr.nbpos.pvalues <- rbind(
  compute.p.values("PropPositionsSecondaryStructure", "Rates", "both"),
  compute.p.values("PropPositionsSecondaryStructure", "RSA", "both"))
ymax <- 120
dat.secstr.nbpos.pvalues$y <- rep(c(0.9, 0.7) * ymax, each = 11)

p.secstr.nbpos <- ggdensity(dat.rnd, x = "PropPositionsSecondaryStructure", fill = "Condition") +
  scale_fill_manual(values = c("Rates" = "black", "RSA" = "grey")) +
  scale_x_continuous(guide = guide_axis(n.dodge = 2), limits = c(0.6, 0.8), breaks = c(0.6, 0.65, 0.7, 0.75, 0.8)) + ylim(0, ymax) +
  geom_vline(data = dat.obs.sum, aes(xintercept = PropPositionsSecondaryStructure, col = Condition)) +
  scale_color_manual(values = c("Rates" = "black", "RSA" = "grey")) +
  geom_label(data = dat.secstr.nbpos.pvalues, aes(x = PropPositionsSecondaryStructure.obsval,
                                         y = y,
                                         color = Condition,
                                         label = paste(sprintf("%.2f", PropPositionsSecondaryStructure.pvalue), " ", PropPositionsSecondaryStructure.psignif, sep = "")),
            hjust = 0, size = 3) +
  facet_wrap(~Method, as.table = FALSE) + theme_pubclean(flip = TRUE) +
  xlab("Proportion of positions in secondary structure") + ylab("Density")
p.secstr.nbpos
# Only I3, Vol, Pol and Charge show a significant bias. For I3 and Volume, there are less positions than expected in secondary structure.
# For polarity, this is only significant when conditioning on rate.
# For charge, there is a significant excess.

dat.secstr.subg.pvalues <- rbind(
  compute.p.values("MeanNbSubgraphsSecondaryStructure", "Rates", "both"),
  compute.p.values("MeanNbSubgraphsSecondaryStructure", "RSA", "both"))
ymax <- 150
dat.secstr.subg.pvalues$y <- rep(c(0.9, 0.7) * ymax, each = 11)

p.secstr.subg <- ggdensity(dat.rnd, x = "MeanNbSubgraphsSecondaryStructure", fill = "Condition") +
  scale_fill_manual(values = c("Rates" = "black", "RSA" = "grey")) +
  scale_x_continuous(guide = guide_axis(n.dodge = 2), limits = c(0.7, 1.05), breaks = c(0.7, 0.8, 0.9, 1.0)) + ylim(0, ymax) +
  geom_vline(data = dat.obs.sum, aes(xintercept = MeanNbSubgraphsSecondaryStructure, col = Condition)) +
  scale_color_manual(values = c("Rates" = "black", "RSA" = "grey")) +
  geom_label(data = dat.secstr.subg.pvalues, aes(x = MeanNbSubgraphsSecondaryStructure.obsval,
                                                 y = y,
                                                 color = Condition,
                                                 label = paste(sprintf("%.2f", MeanNbSubgraphsSecondaryStructure.pvalue), " ", MeanNbSubgraphsSecondaryStructure.psignif, sep = "")),
             hjust = 0, size = 3) +
  facet_wrap(~Method, as.table = FALSE) + theme_pubclean(flip = TRUE) +
  xlab("(Number of graphs - 1) / (Group size - 1)") + ylab("Density")
p.secstr.subg
# Always significantly lower than expected.

p.secstr <- plot_grid(p.secstr.nbpos, p.secstr.subg, labels = "AUTO", nrow = 1)
p.secstr

# Less positions in secondary structure than expected by chance for most methods. More with charge. Several methods non-significant when controling for rate.
# Residues to be more often in the same motif than expected by chance.
ggsave(p.secstr, filename = "SecondaryStructure-All.pdf", width = 12, height = 6)

# Helices:
dat.helix.nbpos.pvalues <- rbind(
  compute.p.values("PropPositionsHelix", "Rates", "both"),
  compute.p.values("PropPositionsHelix", "RSA", "both"))
ymax <- 120
dat.helix.nbpos.pvalues$y <- rep(c(0.9, 0.7) * ymax, each = 11)

p.helix.nbpos <- ggdensity(dat.rnd, x = "PropPositionsHelix", fill = "Condition") +
  scale_fill_manual(values = c("Rates" = "black", "RSA" = "grey")) +
  scale_x_continuous(guide = guide_axis(n.dodge = 2), limits = c(0.4, 0.7), breaks = c(0.6, 0.65, 0.7, 0.75, 0.8)) + ylim(0, ymax) +
  geom_vline(data = dat.obs.sum, aes(xintercept = PropPositionsHelix, col = Condition)) +
  scale_color_manual(values = c("Rates" = "black", "RSA" = "grey")) +
  geom_label(data = dat.helix.nbpos.pvalues, aes(x = PropPositionsHelix.obsval,
                                                 y = y,
                                                 color = Condition,
                                                 label = paste(sprintf("%.2f", PropPositionsHelix.pvalue), " ", PropPositionsHelix.psignif, sep = "")),
             hjust = 0, size = 3) +
  facet_wrap(~Method, as.table = FALSE) + theme_pubclean(flip = TRUE) +
  xlab("Proportion of positions in helices") + ylab("Density")
p.helix.nbpos
# Globally less position than expected in all methods but Charge. Not significant for all properties though.
# Polarity is not significant.

dat.helix.subg.pvalues <- rbind(
  compute.p.values("MeanNbSubgraphsHelix", "Rates", "both"),
  compute.p.values("MeanNbSubgraphsHelix", "RSA", "both"))
ymax <- 60
dat.helix.subg.pvalues$y <- rep(c(0.9, 0.7) * ymax, each = 11)

p.helix.subg <- ggdensity(dat.rnd, x = "MeanNbSubgraphsHelix", fill = "Condition") +
  scale_fill_manual(values = c("Rates" = "black", "RSA" = "grey")) +
  scale_x_continuous(guide = guide_axis(n.dodge = 2), limits = c(0.6, 1.09), breaks = c(0.6, 0.7, 0.8, 0.9, 1.0)) + ylim(0, ymax) +
  geom_vline(data = dat.obs.sum, aes(xintercept = MeanNbSubgraphsHelix, col = Condition)) +
  scale_color_manual(values = c("Rates" = "black", "RSA" = "grey")) +
  geom_label(data = dat.helix.subg.pvalues, aes(x = MeanNbSubgraphsHelix.obsval,
                                                y = y,
                                                color = Condition,
                                                label = paste(sprintf("%.2f", MeanNbSubgraphsHelix.pvalue), " ", MeanNbSubgraphsHelix.psignif, sep = "")),
             hjust = 0, size = 3) +
  facet_wrap(~Method, as.table = FALSE) + theme_pubclean(flip = TRUE) +
  xlab("(Number of graphs - 1) / (Group size - 1)") + ylab("Density")
p.helix.subg
# Significantly lower than expected by chance in most cases, in particular with RSA.

p.helix <- plot_grid(p.helix.nbpos, p.helix.subg, labels = "AUTO", nrow = 1)
p.helix

# Conclusions globally similar, but even less significant proportion of residues when controlling for rate.
ggsave(p.secstr, filename = "SecondaryStructure-Helix.pdf", width = 12, height = 6)

# Strand:

dat.strand.nbpos.pvalues <- rbind(
  compute.p.values("PropPositionsStrand", "Rates", "both"),
  compute.p.values("PropPositionsStrand", "RSA", "both"))
ymax <- 120
dat.strand.nbpos.pvalues$y <- rep(c(0.9, 0.7) * ymax, each = 11)

p.strand.nbpos <- ggdensity(dat.rnd, x = "PropPositionsStrand", fill = "Condition") +
  scale_fill_manual(values = c("Rates" = "black", "RSA" = "grey")) +
  scale_x_continuous(guide = guide_axis(n.dodge = 2), limits = c(0.1, 0.3), breaks = c(0.1, 0.15, 0.2, 0.25, 0.3)) + ylim(0, ymax) +
  geom_vline(data = dat.obs.sum, aes(xintercept = PropPositionsStrand, col = Condition)) +
  scale_color_manual(values = c("Rates" = "black", "RSA" = "grey")) +
  geom_label(data = dat.strand.nbpos.pvalues, aes(x = PropPositionsStrand.obsval,
                                                 y = y,
                                                 color = Condition,
                                                 label = paste(sprintf("%.2f", PropPositionsStrand.pvalue), " ", PropPositionsStrand.psignif, sep = "")),
             hjust = 0, size = 3) +
  facet_wrap(~Method, as.table = FALSE) + theme_pubclean(flip = TRUE) +
  xlab("Proportion of positions in strands") + ylab("Density")
p.strand.nbpos
# Mixed results, most not significant.

dat.strand.subg.pvalues <- rbind(
  compute.p.values("MeanNbSubgraphsStrand", "Rates", "both"),
  compute.p.values("MeanNbSubgraphsStrand", "RSA", "both"))
ymax <- 50
dat.strand.subg.pvalues$y <- rep(c(0.9, 0.7) * ymax, each = 11)

p.strand.subg <- ggdensity(dat.rnd, x = "MeanNbSubgraphsStrand", fill = "Condition") +
  scale_fill_manual(values = c("Rates" = "black", "RSA" = "grey")) +
  scale_x_continuous(guide = guide_axis(n.dodge = 2), limits = c(0.6, 0.9), breaks = c(0.6, 0.7, 0.8, 0.9)) + ylim(0, ymax) +
  geom_vline(data = dat.obs.sum, aes(xintercept = MeanNbSubgraphsStrand, col = Condition)) +
  scale_color_manual(values = c("Rates" = "black", "RSA" = "grey")) +
  geom_label(data = dat.strand.subg.pvalues, aes(x = MeanNbSubgraphsStrand.obsval,
                                                 y = y,
                                                 color = Condition,
                                                 label = paste(sprintf("%.2f", MeanNbSubgraphsStrand.pvalue), " ", MeanNbSubgraphsStrand.psignif, sep = "")),
             hjust = 0, size = 3) +
  facet_wrap(~Method, as.table = FALSE) + theme_pubclean(flip = TRUE) +
  xlab("(Number of graphs - 1) / (Group size - 1)") + ylab("Density")
p.strand.subg
# Generally lower than expected, but not very significant. Charge not significant.

dat.sheet.subg.pvalues <- rbind(
  compute.p.values("MeanNbSubgraphsSheet", "Rates", "both"),
  compute.p.values("MeanNbSubgraphsSheet", "RSA", "both"))
ymax <- 50
dat.sheet.subg.pvalues$y <- rep(c(0.9, 0.7) * ymax, each = 11)

p.sheet.subg <- ggdensity(dat.rnd, x = "MeanNbSubgraphsSheet", fill = "Condition") +
  scale_fill_manual(values = c("Rates" = "black", "RSA" = "grey")) +
  scale_x_continuous(guide = guide_axis(n.dodge = 2), limits = c(0.2, 0.6), breaks = c(0.2, 0.3, 0.4, 0.5, 0.6)) + ylim(0, ymax) +
  geom_vline(data = dat.obs.sum, aes(xintercept = MeanNbSubgraphsSheet, col = Condition)) +
  scale_color_manual(values = c("Rates" = "black", "RSA" = "grey")) +
  geom_label(data = dat.sheet.subg.pvalues, aes(x = MeanNbSubgraphsSheet.obsval,
                                                y = y,
                                                color = Condition,
                                                label = paste(sprintf("%.2f", MeanNbSubgraphsSheet.pvalue), " ", MeanNbSubgraphsSheet.psignif, sep = "")),
             hjust = 0, size = 3) +
  facet_wrap(~Method, as.table = FALSE) + theme_pubclean(flip = TRUE) +
  xlab("(Number of graphs - 1) / (Group size - 1)") + ylab("Density")
p.sheet.subg
# Significantly lower than expected in most cases.

p.strand <- plot_grid(p.strand.nbpos, p.strand.subg, p.sheet.subg, labels = "AUTO", nrow = 1)
p.strand

ggsave(p.strand, filename = "SecondaryStructure-StrandAndSheet.pdf", width = 18, height = 6)


# Conditioning on secondary structure:

perm.helix <- get.perm.data("helix")
dat.rnd.helix <- perm.helix$data
dat.rnd.helix.index <- perm.helix$index

perm.rates.helix <- get.perm.data("rate10-helix")
dat.rnd.rates.helix <- perm.rates.helix$data
dat.rnd.rates.helix.index <- perm.rates.helix$index

perm.rsa.helix <- get.perm.data("rsa10-helix")
dat.rnd.rsa.helix <- perm.rsa.helix$data
dat.rnd.rsa.helix.index <- perm.rsa.helix$index

perm.strand <- get.perm.data("strand")
dat.rnd.strand <- perm.strand$data
dat.rnd.strand.index <- perm.strand$index

perm.rates.strand <- get.perm.data("rate10-strand")
dat.rnd.rates.strand <- perm.rates.strand$data
dat.rnd.rates.strand.index <- perm.rates.strand$index

perm.rsa.strand <- get.perm.data("rsa10-strand")
dat.rnd.rsa.strand <- perm.rsa.strand$data
dat.rnd.rsa.strand.index <- perm.rsa.strand$index

# Check:
table(dat.rnd.helix$PropPositionsHelix)
table(dat.rnd.rates.helix$PropPositionsHelix)
table(dat.rnd.rsa.helix$PropPositionsHelix)
table(dat.rnd.strand$PropPositionsHelix)
table(dat.rnd.rates.strand$PropPositionsHelix)
table(dat.rnd.rsa.strand$PropPositionsHelix)

table(dat.rnd.helix$PropPositionsStrand)
table(dat.rnd.rates.helix$PropPositionsStrand)
table(dat.rnd.rsa.helix$PropPositionsStrand)
table(dat.rnd.strand$PropPositionsStrand)
table(dat.rnd.rates.strand$PropPositionsStrand)
table(dat.rnd.rsa.strand$PropPositionsStrand)

# merge the two:
dat.rnd.helix$Condition <- "None"
dat.rnd.rates.helix$Condition <- "Rates"
dat.rnd.rsa.helix$Condition <- "RSA"
dat.rnd.helix <- rbind(dat.rnd.helix, dat.rnd.rates.helix, dat.rnd.rsa.helix)
write.csv(dat.rnd.helix, file = "RandomizedDataHelix.csv", row.names = FALSE)

dat.rnd.strand$Condition <- "None"
dat.rnd.rates.strand$Condition <- "Rates"
dat.rnd.rsa.strand$Condition <- "RSA"
dat.rnd.strand <- rbind(dat.rnd.strand, dat.rnd.rates.strand, dat.rnd.rsa.strand)
write.csv(dat.rnd.strand, file = "RandomizedDataStrand.csv", row.names = FALSE)

# Filter the observed groups for which permutations could be obtained:
dat.obs.helix <- filter.observed.data(dat.obs, dat.rnd.helix.index) #0% groups filtered out.
dat.obs.rates.helix <- filter.observed.data(dat.obs, dat.rnd.rates.helix.index) #0.3402946% groups filtered out.
dat.obs.rsa.helix <- filter.observed.data(dat.obs, dat.rnd.rsa.helix.index) #35.7356% groups filtered out.
dat.obs.strand <- filter.observed.data(dat.obs, dat.rnd.strand.index) #0% groups filtered out.
dat.obs.rates.strand <- filter.observed.data(dat.obs, dat.rnd.rates.strand.index) #0.6666045% groups filtered out.
dat.obs.rsa.strand <- filter.observed.data(dat.obs, dat.rnd.rsa.strand.index) #46.1309% groups filtered out.

dat.obs.helix.sum <- summarise.observed.data(dat.obs.helix)
dat.obs.rates.helix.sum <- summarise.observed.data(dat.obs.rates.helix)
dat.obs.rsa.helix.sum <- summarise.observed.data(dat.obs.rsa.helix)
dat.obs.strand.sum <- summarise.observed.data(dat.obs.strand)
dat.obs.rates.strand.sum <- summarise.observed.data(dat.obs.rates.strand)
dat.obs.rsa.strand.sum <- summarise.observed.data(dat.obs.rsa.strand)

# merge the three:
dat.obs.helix.sum$Condition <- "None"
dat.obs.rates.helix.sum$Condition <- "Rates"
dat.obs.rsa.helix.sum$Condition <- "RSA"
dat.obs.helix.sum <- as.data.frame(rbind(dat.obs.helix.sum, dat.obs.rates.helix.sum, dat.obs.rsa.helix.sum))

dat.obs.strand.sum$Condition <- "None"
dat.obs.rates.strand.sum$Condition <- "Rates"
dat.obs.rsa.strand.sum$Condition <- "RSA"
dat.obs.strand.sum <- as.data.frame(rbind(dat.obs.strand.sum, dat.obs.rates.strand.sum, dat.obs.rsa.strand.sum))

# Plot results:

# Helices:
dat.helix.subg.pvalues2 <- rbind(
  compute.p.values("MeanNbSubgraphsHelix", "None", "both", dat.rnd.helix, dat.obs.helix.sum),
  compute.p.values("MeanNbSubgraphsHelix", "Rates", "both", dat.rnd.helix, dat.obs.helix.sum),
  compute.p.values("MeanNbSubgraphsHelix", "RSA", "both", dat.rnd.helix, dat.obs.helix.sum))
ymax <- 120
dat.helix.subg.pvalues2$y <- rep(c(0.9, 0.7, 0.5) * ymax, each = 11)

p.helix.subg2 <- ggdensity(dat.rnd.helix, x = "MeanNbSubgraphsHelix", fill = "Condition") +
  scale_fill_brewer(type = "qual", palette = 2) +
  scale_x_continuous(guide = guide_axis(n.dodge = 2), limits = c(0.6, 1.0), breaks = c(0.6, 0.7, 0.8, 0.9, 1.0)) + ylim(0, ymax) +
  geom_vline(data = dat.obs.helix.sum, aes(xintercept = MeanNbSubgraphsHelix, col = Condition)) +
  scale_color_brewer(type = "qual", palette = 2) +
  geom_label(data = dat.helix.subg.pvalues2, aes(x = MeanNbSubgraphsHelix.obsval,
                                                 y = y,
                                                 color = Condition,
                                                 label = paste(sprintf("%.2f", MeanNbSubgraphsHelix.pvalue), " ", MeanNbSubgraphsHelix.psignif, sep = "")),
             hjust = 0, size = 3) +
  facet_wrap(~Method, as.table = FALSE) + theme_pubclean(flip = TRUE) +
  xlab("(Number of graphs - 1) / (Group size - 1)") + ylab("Density")
p.helix.subg2

# Strands:
dat.strand.subg.pvalues2 <- rbind(
  compute.p.values("MeanNbSubgraphsStrand", "None", "both", dat.rnd.strand, dat.obs.strand.sum),
  compute.p.values("MeanNbSubgraphsStrand", "Rates", "both", dat.rnd.strand, dat.obs.strand.sum),
  compute.p.values("MeanNbSubgraphsStrand", "RSA", "both", dat.rnd.strand, dat.obs.strand.sum))
ymax <- 30
dat.strand.subg.pvalues2$y <- rep(c(0.9, 0.7, 0.5) * ymax, each = 11)

p.strand.subg2 <- ggdensity(dat.rnd.strand, x = "MeanNbSubgraphsStrand", fill = "Condition") +
  scale_fill_brewer(type = "qual", palette = 2) +
  scale_x_continuous(guide = guide_axis(n.dodge = 2), limits = c(0.5, 1.0), breaks = c(0.5, 0.6, 0.7, 0.8, 0.9, 1.0)) + ylim(0, ymax) +
  geom_vline(data = dat.obs.strand.sum, aes(xintercept = MeanNbSubgraphsStrand, col = Condition)) +
  scale_color_brewer(type = "qual", palette = 2) +
  geom_label(data = dat.strand.subg.pvalues2, aes(x = MeanNbSubgraphsStrand.obsval,
                                                  y = y,
                                                  color = Condition,
                                                  label = paste(sprintf("%.2f", MeanNbSubgraphsStrand.pvalue), " ", MeanNbSubgraphsStrand.psignif, sep = "")),
             hjust = 0, size = 3) +
  facet_wrap(~Method, as.table = FALSE) + theme_pubclean(flip = TRUE) +
  xlab("(Number of graphs - 1) / (Group size - 1)") + ylab("Density")
p.strand.subg2

# Conclusion: no significant difference, no more often in the same strand than expected by chance

# Sheets:
dat.sheet.subg.pvalues2 <- rbind(
  compute.p.values("MeanNbSubgraphsSheet", "None", "both", dat.rnd.strand, dat.obs.strand.sum),
  compute.p.values("MeanNbSubgraphsSheet", "Rates", "both", dat.rnd.strand, dat.obs.strand.sum),
  compute.p.values("MeanNbSubgraphsSheet", "RSA", "both", dat.rnd.strand, dat.obs.strand.sum))
ymax <- 30
dat.sheet.subg.pvalues2$y <- rep(c(0.9, 0.7, 0.5) * ymax, each = 11)

p.sheet.subg2 <- ggdensity(dat.rnd.strand, x = "MeanNbSubgraphsSheet", fill = "Condition") +
  scale_fill_brewer(type = "qual", palette = 2) +
  scale_x_continuous(guide = guide_axis(n.dodge = 2), limits = c(0.0, 0.7), breaks = c(0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7)) + ylim(0, ymax) +
  geom_vline(data = dat.obs.strand.sum, aes(xintercept = MeanNbSubgraphsSheet, col = Condition)) +
  scale_color_brewer(type = "qual", palette = 2) +
  geom_label(data = dat.sheet.subg.pvalues2, aes(x = MeanNbSubgraphsSheet.obsval,
                                                 y = y,
                                                 color = Condition,
                                                 label = paste(sprintf("%.2f", MeanNbSubgraphsSheet.pvalue), " ", MeanNbSubgraphsSheet.psignif, sep = "")),
             hjust = 0, size = 3) +
  facet_wrap(~Method, as.table = FALSE) + theme_pubclean(flip = TRUE) +
  xlab("(Number of graphs - 1) / (Group size - 1)") + ylab("Density")
p.sheet.subg2
# Conclusion: more often in the same sheet when coevolving for charge.

p2 <- plot_grid(p.helix.subg2, NULL, p.strand.subg2, p.sheet.subg2, labels = c("A: helices", "", "B: strands", "C: sheets"), nrow = 2)
p2

ggsave(p2, filename = "SecondaryStructureConditionned.pdf", width = 12, height = 12)

